using UnityEngine;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
    internal class DummyClient : IGoogleMobileAdsBannerClient, IGoogleMobileAdsInterstitialClient
    {
        public DummyClient(IAdListener listener)
        {
            BossDebug.Log("Created DummyClient", BossLogType._Ads);
        }

        public void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position)
        {
            BossDebug.Log("Dummy CreateBannerView", BossLogType._Ads);
        }

        public void LoadAd(AdRequest request)
        {
            BossDebug.Log("Dummy LoadAd", BossLogType._Ads);
        }

        public void ShowBannerView()
        {
            BossDebug.Log("Dummy ShowBannerView", BossLogType._Ads);
        }

        public void HideBannerView()
        {
            BossDebug.Log("Dummy HideBannerView", BossLogType._Ads);
        }

        public void DestroyBannerView()
        {
            BossDebug.Log("Dummy DestroyBannerView", BossLogType._Ads);
        }

        public void CreateInterstitialAd(string adUnitId) {
            BossDebug.Log("Dummy CreateIntersitialAd", BossLogType._Ads);
        }

        public bool IsLoaded() {
            BossDebug.Log("Dummy IsLoaded", BossLogType._Ads);
            return true;
        }

        public void ShowInterstitial() {
            BossDebug.Log("Dummy ShowInterstitial", BossLogType._Ads);
        }

        public void DestroyInterstitial() {
            BossDebug.Log("Dummy DestroyInterstitial", BossLogType._Ads);
        }
    }
}
