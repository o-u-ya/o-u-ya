﻿using UnityEngine;
using System.Collections;

public class AppManager : Boss.MonoSingleton<AppManager> 
{
    public override void Init()
    {
        base.Init();
        //PageManager.Instance.Open(ePageType._Intro_IamBoss, eNextPageEffect._Fade_InOut);
		PageManager.Instance.Open(ePageType._Loading);
    }
}