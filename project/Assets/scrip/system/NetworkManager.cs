﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text.RegularExpressions;


public class NetworkManager : Boss.MonoSingleton<NetworkManager> 
{
	private string url = "http://1.245.135.45/o-u-ya/";
	public const string RESULT_SUCCESS		= "100";
	public const string RESULT_ERROR		= "101";
	public const string RESULT_DO_NOTHING	= "102";
	public const string RESULT_EMPTY_PARAMS	= "201";
	public const string RESULT_QUERY_ERROR	= "301";
	public const string RESULT_NODATA		= "NO_RETURN_DATA";

	private Queue<Message> que = new Queue<Message>();
	private int? id = null;
	public delegate void CallBack_Success(string result, JSONNode data);
	public delegate void CallBack_Error(string result);

	private class Message
	{
		public string		url;
		public Hashtable	args;
		public CallBack_Success		CallBack_Success;
		public CallBack_Error		CallBack_Error;

		public Message(string url, Hashtable args, CallBack_Success CallBack_Success,
			CallBack_Error CallBack_Error)
		{
			this.url = url;
			this.args = args;
			this.CallBack_Success = CallBack_Success;
			this.CallBack_Error = CallBack_Error;
		}
	}

    public override void Init()
    {
        base.Init();
    }

	public void Send(string url, Hashtable args,
		CallBack_Success CallBack_Success = null,
		CallBack_Error CallBack_Error = null)
	{
		if ((id != null) && !args.ContainsKey("id")) args.Add("id", id);
		que.Enqueue(new Message(url, args, CallBack_Success, CallBack_Error));
		if (que.Count == 1) StartCoroutine(Send(que.Peek()));
		//else Debug.Log(que.Count);
	}

	IEnumerator Send(Message Message)
	{
		WWWForm form = new WWWForm();
		foreach (DictionaryEntry This in Message.args)
		{
			form.AddField(This.Key.ToString(), This.Value.ToString());
		}

		if ((BossDebug.Print & (long)BossLogType._Net_Send) != 0)
		{
			string log = "[Send] to:"+Message.url+", params:{";
			foreach (DictionaryEntry This in Message.args)
				log += This.Key.ToString()+":"+This.Value.ToString()+", ";
			log = log.Substring(0, log.Length - 2);
			log += '}';
			BossDebug.Log(log, BossLogType._Net_Send);
		}

		WWW www = new WWW(url+Message.url+".php", form);
		yield return www;

		//실패
		if (string.IsNullOrEmpty(www.error) == false)
		{
			BossDebug.LogError(www.error);
			if (Message.CallBack_Error != null)
				Message.CallBack_Error(www.error);
			Next();
			yield break;
		}

		string text = www.text.Substring(Regex.Match(www.text, "[a-zA-Z0-9]").Index);
		string result = string.IsNullOrEmpty(text) ? RESULT_NODATA : text.Substring(0, 3);
		string data = text.Substring(4);
		if ((BossDebug.Print & (long)BossLogType._Net_Get) != 0)
		{
			string log = "[Get] from:"+Message.url+", result:"+result+", data:"+data;
			BossDebug.Log(log, BossLogType._Net_Get);
		}
		if (Message.CallBack_Success != null)
			Message.CallBack_Success(result, JSON.Parse(data));
		Next();
		yield break;
	}

	private void Next()
	{
		que.Dequeue();
		if (que.Count > 0) StartCoroutine(Send(que.Peek()));
	}

	public void SetId(int id) {this.id = id;}
}
