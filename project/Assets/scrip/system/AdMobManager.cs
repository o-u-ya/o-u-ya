﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds.Api;

/// <summary>
/// RCH
/// </summary>
public class AdMobManager : Boss.MonoSingleton<AdMobManager>
{
    // admob에서 부여받은 id
#if UNITY_ANDROID
    string ADUnitID = "ca-app-pub-7409722894442665/2391350632";
#elif UNITY_IPHONE
    string ADUnitID = "iphone id";
#else
    string ADUnitID = "unexpected_platform";
#endif

    BannerView bannerView;
    InterstitialAd interstitial;

    public override void Init()
    {
        base.Init();

        CreateBanner();
        CreateInterstitial();
        RequestBanner();
    }

    /// <summary>
    /// 배너생성
    /// </summary>
    void CreateBanner()
    {
        
        bannerView = new BannerView(ADUnitID, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
    }

    /// <summary>
    /// 전면광고 생성
    /// </summary>
    void CreateInterstitial()
    {
        interstitial = new InterstitialAd(ADUnitID);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }

    /// <summary>
    /// 배너 보이기
    /// </summary>
    public void ShowBannerView()
    {
        Debug.Log("ShowBannerView");
        bannerView.Show();
    }

    /// <summary>
    /// 배너 숨기기
    /// </summary>
    public void HideBannerView()
    {
        
        bannerView.Hide();
    }

    /// <summary>
    /// 전면광고 보이기
    /// </summary>
    public void ShowInterstitial()
    {
        // 전체 광고 보이기 
        // 로드가 완료되어야 보인다.
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    /// <summary>
    /// Callback
    /// </summary>
    private void RequestBanner()
    {
        BannerView bannerView = new BannerView(ADUnitID, AdSize.Banner, AdPosition.Top);
        // Called when an ad request has successfully loaded.
        bannerView.AdLoaded += HandleAdLoaded;
        // Called when an ad request failed to load.
        bannerView.AdFailedToLoad += HandleAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.AdOpened += HandleAdOpened;
        // Called when the user is about to return to the app after an ad click.
        bannerView.AdClosing += HandleAdClosing;
        // Called when the user returned from the app after an ad click.
        bannerView.AdClosed += HandleAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.AdLeftApplication += HandleAdLeftApplication;
    }



    public void HandleAdLoaded(object sender, EventArgs args)
    {

    }

    public void HandleAdFailedToLoad(object sender, EventArgs args)
    {

    }

    public void HandleAdOpened(object sender, EventArgs args)
    {

    }

    public void HandleAdClosing(object sender, EventArgs args)
    {

    }

    public void HandleAdClosed(object sender, EventArgs args)
    {

    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {

    }

    public void DestroyAds()
    {
        // 광고를 파괴한다!
        bannerView.Destroy();
        interstitial.Destroy();
    }

}
