﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : Boss.MonoSingleton<SoundManager> {

	private class PlayingClip {
		public string	key;
		public int		id;
	}

	private SoundData soundData;
	private Dictionary<string, List<AudioClip>> soundDict = new Dictionary<string,List<AudioClip>>();
	private AudioSource audioSource;
	private PlayingClip playingClip = new PlayingClip();

    public override void Init() {
        base.Init();
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.loop = true;
		audioSource.volume = 0.5f;
	}

	public void Load() {
		soundData = BossUtil.XmlToClass<SoundData>("SoundData");
		foreach (SoundDataItem This in soundData.Items) {
			List<AudioClip> clips = new List<AudioClip>();
			string[] items = {
				This.item0, This.item1, This.item2, This.item3, This.item4, This.item5
			};
			foreach (string item in items) {
				if (string.Compare(item, Defines.None) == 0) break;
				clips.Add(Resources.Load(Defines.SOUND_PATH + item) as AudioClip);
			}
			soundDict.Add(This.key, clips);
		}
	}

	public void PlayBGM(string key, int id = -1) {
		if (soundDict.ContainsKey(key) == false) return;
		if (audioSource.isPlaying) audioSource.Stop();

		if (id != -1) {
			playingClip.id = id;
		}
		else if (string.Compare(playingClip.key, key) == 0 &&
			soundDict[key].Count > 1) {
			while (true) {
				int temp = Random.Range(0, soundDict[key].Count);
				if (playingClip.id != temp) {
					playingClip.id = temp;
					break;
				}
			}
		}
		else {
			playingClip.id = Random.Range(0, soundDict[key].Count);
		}
		playingClip.key = key;

		audioSource.clip = soundDict[ playingClip.key ][ playingClip.id ];
		audioSource.Play();
	}

	public void PlayEffect(string key) {
		GameObject obj = new GameObject();
		obj.transform.parent = gameObject.transform;
		obj.name = key;

		AudioSource audioSource = obj.AddComponent<AudioSource>();
		audioSource.loop = false;
		audioSource.clip = soundDict[key][Random.Range(0, soundDict[key].Count)];
		audioSource.Play();

		DestroyObject(obj, audioSource.clip.length);
	}
}
