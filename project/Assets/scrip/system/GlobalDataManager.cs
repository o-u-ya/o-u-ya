﻿#define LEVEL

using UnityEngine;
using System.Collections;
using System;


public class GlobalDataManager : Boss.MonoSingleton<GlobalDataManager> 
{
	private GlobalData			data;
    //private MonsterData			monsterData;
#if LEVEL
	private MonsterLevelData	monsterLevelData;
#endif

    public override void Init()
    {
        base.Init();
		data = BossUtil.XmlToClass<GlobalData>("GlobalData");
        //monsterData = BossUtil.XmlToClass<MonsterData>("MonsterData");
#if LEVEL
		monsterLevelData = BossUtil.XmlToClass<MonsterLevelData>("MonsterLevelData");
#endif
    }

	private GlobalDataItem GetItem(string key)
	{
		foreach (GlobalDataItem This in data.Items)
		{
			if (string.IsNullOrEmpty(This.key))
			{
				BossDebug.LogWarning("GlobalData.xml에 key가 비어있는 아이템이 있어요");
				continue;
			}
			if (This.key.Equals(key)) return This;
		}

		BossDebug.LogWarning("아이템을 찾지 못했어요");
		return null;
	}

	public decimal? GetItem(string key, int level)
	{
		if (level < 0)
		{
			BossDebug.LogError("레벨이 0보다 작을 수는 없어요");
		}

		GlobalDataItem item = GetItem(key);
		if (item == null) return null;
		decimal value = decimal.Parse(item.startPoint);
		decimal amp = decimal.Parse(item.addPercent);
		decimal add = decimal.Parse(item.addPoint);
		try 
		{
			for (int i=0 ; i<level ; i++)
			{
				value *= (1 + amp);
				amp += add;
			}
		}
		catch (OverflowException e) 
		{
			BossDebug.LogWarning("이런"+e);
			value = decimal.MaxValue;
		}
		value = decimal.Round(value);
		return value;
	}

	public const string keyStageHp = "stage_hp";
	public const string keyStageCoin = "stage_coin";
	public StageData GetStage(int level)
	{

		decimal? hp;
		decimal? coin;

#if LEVEL
		level = Mathf.Clamp(level, 0, 99);
		hp = decimal.Parse(monsterLevelData.Items[level].hp);
		coin = decimal.Parse(monsterLevelData.Items[level].coin);
#else
		hp = GetItem(keyStageHp, level);
		coin = GetItem(keyStageCoin, level);
#endif

		if ((hp == null) || (coin == null)) return null;

		StageData stageData = new StageData();
		stageData.level = level;
		stageData.hp = hp.Value;
		stageData.coin = coin.Value;
		stageData.step = 1;

		return stageData;
	}
}