﻿using UnityEngine;
using System.Collections;

public class Message : Boss.MonoSingleton<Message> 
{
	private enum eButton {_Yes, _No, _End};
	private enum eLabel {_Box, _End};
	private const string	YesButtonName = "button_yes";
	private const string	NoButtonName = "button_no";
	private const string	BoxLabelName = "label_box";


	private GameObject			m_Obj;		//자기자신
	private GameObject[]		m_Button = new GameObject[(int)eButton._End];
	private UILabel[]			m_Label = new UILabel[(int)eLabel._End];


	public override void Init()
	{
		Transform[]		temp;


		//가져오기
		base.Init();
		m_Obj = PageManager.Instance.Open(ePageType._Message);

		//연결
		temp = m_Obj.GetComponentsInChildren<Transform>();
		foreach (Transform This in temp)
		{
			if (!string.IsNullOrEmpty(This.name))
			{
				switch (This.name)
				{
					case YesButtonName: m_Button[(int)eButton._Yes] = This.gameObject; break;
					case NoButtonName: m_Button[(int)eButton._No] = This.gameObject; break;
					case BoxLabelName: m_Label[(int)eLabel._Box] = This.GetComponent<UILabel>(); break;
				}
			}
		}

		PageManager.Instance.Close(ePageType._Message);
	}

	//
	// 보여주기
	//
	public void Show(string szText,
		UIEventListener.VoidDelegate OnClickYes = null,
		UIEventListener.VoidDelegate OnClickNo = null)
	{
		//m_Hero.LockOnly(true);
		m_Label[(int)eLabel._Box].text = szText;
		PageManager.Instance.Open(ePageType._Message, eNextPageEffect._Pung);

		BossUtil.Button.SetEvent
		(
			m_Button[(int)eButton._Yes],
			delegate
			{
				PageManager.Instance.Close(ePageType._Message, eNextPageEffect._PungReverse);
			}
		);
		BossUtil.Button.SetEvent
		(
			m_Button[(int)eButton._No],
			delegate
			{
				PageManager.Instance.Close(ePageType._Message, eNextPageEffect._PungReverse);
			}
		);

		if (OnClickYes != null)
		{
			BossUtil.Button.AddEvent(m_Button[(int)eButton._Yes], OnClickYes);
		}
		if (OnClickNo != null)
		{
			BossUtil.Button.AddEvent(m_Button[(int)eButton._No], OnClickNo);
		}
	}
}
