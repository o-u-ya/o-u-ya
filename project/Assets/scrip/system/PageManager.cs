﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum ePageType
{
	_Message,
    _Intro_IamBoss, _Intro_UnityChan,
	_Loading,
	_BossLoading,
	_Game,
	_Exit,
    _End
}

public enum eNextPageEffect
{
	_None,
    _Fade_InOut,
	_Pung,
	_PungReverse,
	_Exit,
	_End
}

public class PageManager : Boss.MonoSingleton<PageManager> 
{
	private class Page
    {
        public string	path;
		public bool		onlyOne;	//다른 페이지를 모두 지우고 불러오는 옵션
		public bool		notDestroy;		//파괴하지않고 active를 조절하게.

		public Page(string path, bool onlyOne = false, bool notDestroy = false)
		{
			this.path = path;
			this.onlyOne = onlyOne;
			this.notDestroy = notDestroy;
		}
    }

	private class ReadPage
	{
		public GameObject	obj;
		public bool			show;

		public ReadPage(GameObject obj, bool show = false)
		{
			this.obj = obj;
			this.show = show;
		}
	}

	[SerializeField]
	private string CameraTag = "NGUICamera";

    private Page[]				m_Page = new Page[(int)ePageType._End];
	private NextPageEffect[]	m_Effect = new NextPageEffect[(int)eNextPageEffect._End];
    private GameObject			m_Parent;
	private Dictionary<ePageType, ReadPage> m_ReadPage = new Dictionary<ePageType, ReadPage>();


    //
    // 시작
    //
    public override void Init()
    {
        base.Init();

        SetPage();
        FindParent();
		SetEffect();
    }

    //
    // 이름 입력
    //
    private void SetPage()
    {
        m_Page[(int)ePageType._Message] = new Page("page/system/message", false, true);
        m_Page[(int)ePageType._Intro_IamBoss] = new Page("page/intro/iamboss", true);
        m_Page[(int)ePageType._Intro_UnityChan] = new Page("page/intro/unitychan", true);
		m_Page[(int)ePageType._Loading] = new Page("page/game/loading", true);
		m_Page[(int)ePageType._BossLoading] = new Page("page/game/bossLoading", false);
        m_Page[(int)ePageType._Game] = new Page("page/game/game", true);
        m_Page[(int)ePageType._Exit] = new Page("page/intro/iamboss-bye", true);
    }

    //
    // 페이지를 넣을 부모 찾기
    //
    private void FindParent()
    {
		UICamera[] cameraList = FindObjectsOfType(typeof(UICamera)) as UICamera[];
		foreach (UICamera This in cameraList)
		{
			if ((string.IsNullOrEmpty(This.gameObject.tag) == false) &&
				(This.gameObject.tag.Equals(CameraTag)))
			{
				if (Parent != null)
				{
					BossDebug.LogError("이미 부모가 등록되어있어요. NGUICamrea는 하나만 설정해야해요.");
				}

				else
				{
					m_Parent = This.gameObject;
				}
			}
		}

        IsSetParent();
    }

	//
	// 열려있는 페이지가 있다면 true
	//
	public bool IsHaveReadPage()
	{
		return (m_ReadPage.Count != 0);
	}

	//
	// 온리원
	//
	public bool IsOnlyonePage(ePageType ePageType)
	{
		return m_Page[(int)ePageType].onlyOne;
	}

	//
	// 숨기기
	//
	public bool IsNotDestroyPage(ePageType ePageType)
	{
		return m_Page[(int)ePageType].notDestroy;
	}

	//
	// 부모가 있는지
	//
	public bool IsSetParent()
	{
        if (Parent == null) BossDebug.LogError("부모를 찾지 못했어요.");
		return Parent != null;
	}

	public GameObject Parent
	{
		get {return m_Parent;}
	}

	//
	// 페이지 전환효과 정의
	//
	private void SetEffect() 
	{
		const float timeFadeOut = 1.0f;
		const float timeFadeIn = 1.0f;
		const float timeLogoWait = 1.5f;
		const float timePung = 0.25f;
		const float timePungReverse = 0.15f;

		m_Effect[(int)eNextPageEffect._Fade_InOut] = new FadeInOutEffect("page/effect/fade", timeFadeOut, timeFadeIn);
		m_Effect[(int)eNextPageEffect._Pung] = new PungEffect(timePung);
		m_Effect[(int)eNextPageEffect._PungReverse] = new PungReverseEffect(timePungReverse);
		m_Effect[(int)eNextPageEffect._Exit] = new ExitEffect("page/effect/fade", timeFadeOut, timeFadeIn, timeLogoWait);
	}

    //
    // 페이지 열기
    //
    public GameObject Open(ePageType ePageType, eNextPageEffect eNextPageEffect = eNextPageEffect._None)
    {
        if (IsSetParent() == false) return null;
        GameObject Obj = Resources.Load(m_Page[(int)ePageType].path) as GameObject;
        if (Obj == null)
        {
            BossDebug.LogError(
				"리소스를 찾을 수 없어요. SetPage에 등록한 path를 확인해보세요\n"+
				"path:"+m_Page[(int)ePageType].path+", ePageType:"+ePageType+"\n");
            return null;
        }

		if (eNextPageEffect == eNextPageEffect._None)
		{
			if (IsOnlyonePage(ePageType)) CloseAll();
			if (IsNotDestroyPage(ePageType) && m_ReadPage.ContainsKey(ePageType))
			{
				m_ReadPage[ePageType].show = true;
				NGUITools.SetActive(m_ReadPage[ePageType].obj, true);
				return m_ReadPage[ePageType].obj;
			}

			m_ReadPage.Add(ePageType, new ReadPage(NGUITools.AddChild(Parent, Obj), true));
			return m_ReadPage.Last().Value.obj;
		}
		else
		{
			m_Effect[(int)eNextPageEffect].Show(ePageType);
		}
		return null;
    }

	//
	// 닫기
	//
	public void Close(ePageType ePageType, eNextPageEffect eNextPageEffect = eNextPageEffect._None)
	{
		if (eNextPageEffect == eNextPageEffect._None)
		{
			if (IsNotDestroyPage(ePageType) == false)
			{
				Destroy(m_ReadPage[ePageType].obj);
				m_ReadPage.Remove(ePageType);
			}
			else 
			{
				NGUITools.SetActive(m_ReadPage[ePageType].obj, false);
				m_ReadPage[ePageType].show = false;
			}
		}
		else
		{
			m_Effect[(int)eNextPageEffect].Show(ePageType);
		}
	}

	//
	// 가져오기
	//
	public GameObject Get(ePageType ePageType)
	{
		if ((m_ReadPage.ContainsKey(ePageType) == false) ||
			(m_ReadPage[ePageType].show == false)) return null;
		return m_ReadPage[ePageType].obj;
	}

	//
	// 모든 페이지 닫기
	//
	public void CloseAll()
	{
		foreach (ePageType ePageType in m_ReadPage.Keys.ToList())
		{
			Close(ePageType);
		}
	}
}