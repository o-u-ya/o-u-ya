﻿using UnityEngine;
using System.Collections;


public static class Defines
{
	static public readonly string None = "None";
	static public readonly string SOUND_PATH = "sound/";
	static public readonly string BOARD_PATH = "texture/board/";
	static public readonly string BACK_PATH = "texture/back/";
	static public readonly string MONSTER_PREFABS_PATH = "page/game/monster";
	static public readonly string MONSTER_MATERIAL_PATH = "shader/monster";
	static public readonly string XML_PATH = "xml/";
	static public readonly string PARTICLE_PATH = "particle/";
	static public readonly float BOARD_SHAKE_FORCE = 0.05f;
	static public readonly float BOARD_SHAKE_TIME = 0.5f;

	static public readonly float TIME_DIE = 1.0f;

	static public readonly float	TIMEOVER = 30.0f;
	static public readonly float	TIMEALERT = 5.0f;
	static public readonly int		MONSTER_STEP = 4;
	static public readonly int		BACK_LEVEL = 25;

	static public readonly int MASKSIZEX = 1920 / 4;
	static public readonly int MASKSIZEY = 1080 / 4;
	static public readonly int MASKSIZE = MASKSIZEY;
}

public class StageData
{
	public int		level;
	public int		step;
	public decimal	hp;
	public decimal	coin;
}

public class InventoryData
{
    public string itemgroup { get; set; }
    public string itemid { get; set; }
    public string itemlevel { get; set; }
    public string usedtype { get; set; } 
	//public decimal total { get; set; }
}