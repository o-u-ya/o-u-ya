﻿using UnityEngine;
using System.Collections;

public class SingletonManager : MonoBehaviour 
{
	void Start()
	{
		CoroutineManager CoroutineManager = CoroutineManager.Instance;
		SoundManager SoundManager = SoundManager.Instance;
        PageManager PageManager = PageManager.Instance;
		Message Message = Message.Instance;
		NetworkManager NetworkManager = NetworkManager.Instance;
        AppManager AppManager = AppManager.Instance;
        AdMobManager AdmobManager = AdMobManager.Instance;
        Inventory InventoryManager = Inventory.Instance;
	}
}