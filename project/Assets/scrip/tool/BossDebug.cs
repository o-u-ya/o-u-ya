﻿using UnityEngine;
using System.Collections;

public enum BossLogType
{
	_Default	= 0x00000001,
	_Ads		= 0x00000002,
	_Net_Send	= 0x00000004,
	_Net_Get	= 0x00000008,
	
	_Net_All	= _Net_Send | _Net_Get,
	_All		= _Default | _Ads | _Net_All
};

public class BossDebug : MonoBehaviour
{
	//static public long Print = (long)BossLogType._All;
	static public long Print = (long)BossLogType._Default | (long)BossLogType._Net_All;

	static private string MakeColorWord(BossLogType LogType, string buf)
	{
		switch (LogType)
		{
			case BossLogType._Net_Send:	return "<color=#99CC00>"+buf+"</color>";
			case BossLogType._Net_Get:	return "<color=#9999FF>"+buf+"</color>";
			default:					return buf;
		}
	}

    static public void Log(object buf, BossLogType LogType = BossLogType._Default)
	{
		if ((Print & (long)LogType) == 0) return;
		Debug.Log(MakeColorWord(LogType, buf.ToString()));
	}
	static public void LogWarning(object buf, BossLogType LogType = BossLogType._Default)
	{
		if ((Print & (long)LogType) == 0) return;
		Debug.LogWarning(MakeColorWord(LogType, buf.ToString()));
	}
    static public void LogError(object buf, BossLogType LogType = BossLogType._Default)
	{
		if ((Print & (long)LogType) == 0) return;
		Debug.LogError(MakeColorWord(LogType, buf.ToString()));
	}
}