﻿using UnityEngine;
using System.Collections;

namespace Boss
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        private static T m_Instance = null;
        public static T Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = GameObject.FindObjectOfType(typeof(T)) as T;
                    if (m_Instance == null)
                    {
                        m_Instance = new GameObject("Temp Instance of " + typeof(T).ToString(), typeof(T)).GetComponent<T>();
                        //DontDestroyOnLoad(m_Instance);
                        if (m_Instance == null)
                        {
                            Debug.LogError("MonoSingletone 만들지 못했어요. [" + typeof(T).ToString() + "]");
                        }
                    }
                }
                return m_Instance;
            }
        }

        protected virtual void Start() {}

        protected virtual void Awake()
        {
            if (m_Instance == null)
            {
                m_Instance = this as T;
				//DontDestroyOnLoad(m_Instance);

                m_Instance.Init();
            }
            else
            {
                Object.DestroyImmediate(gameObject);
            }
        }

        public virtual void Init()
		{
		}
    }
}