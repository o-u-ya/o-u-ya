﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

public class BossUtil : MonoBehaviour 
{
	public static class Button
	{
		static private void OnPressPung(GameObject obj, bool press)
		{
			float amount = 0.5f;
			float time = 1.0f;

			if (!press) return;
			SoundManager.Instance.PlayEffect("button");
			NGUITools.DestroyImmediate(obj.GetComponent(typeof(iTween)));
			obj.transform.localScale = Vector3.one;
			iTween.PunchScale(obj, new Vector3(amount, amount, amount), time);
		}

		static private void EventClear(GameObject obj)
		{
			UIEventListener.Get(obj).onPress = null;
			UIEventListener.Get(obj).onClick = null;
		}

		static public void AddEvent(GameObject obj, UIEventListener.VoidDelegate OnClick)
		{
			UIEventListener.Get(obj).onPress = OnPressPung;
			UIEventListener.Get(obj).onClick += OnClick;
		}

		static public void RemoveEvent(GameObject obj, UIEventListener.VoidDelegate OnClick)
		{
			UIEventListener.Get(obj).onPress = null;
			UIEventListener.Get(obj).onClick -= OnClick;
		}

		static public void SetEvent(GameObject obj, UIEventListener.VoidDelegate OnClick)
		{
			UIEventListener.Get(obj).onPress = OnPressPung;
			UIEventListener.Get(obj).onClick = OnClick;
		}
	}

	static public class Animation
	{
		static public readonly float coinAnimTime = 0.65f;
		static private bool coinAnimFin = true;

		static public void Pung(GameObject obj, float amount = 0.5f)
		{
			float time = 1.0f;

			NGUITools.DestroyImmediate(obj.GetComponent(typeof(iTween)));
			obj.transform.localScale = Vector3.one;
			iTween.PunchScale(obj, new Vector3(amount, amount, amount), time);
		}

		static public void Particle(Vector2 pos, string path)
		{
			GameObject obj = Resources.Load(Defines.PARTICLE_PATH+path) as GameObject;
			obj = GameObject.Instantiate(obj) as GameObject;
			obj.transform.localPosition = pos;
			obj.transform.localRotation = Quaternion.identity;
			obj.transform.localScale = Vector3.one;
			obj.transform.parent = PageManager.Instance.Parent.transform;
		}

		static public GameObject Particle(Vector2 pos, GameObject particle, bool isLocal = true)
		{
			//GameObject obj = Resources.Load(Defines.PARTICLE_PATH+path) as GameObject;
			GameObject obj = GameObject.Instantiate(particle) as GameObject;
			if (!isLocal) obj.transform.localPosition = pos;
			obj.transform.parent = PageManager.Instance.Parent.transform;
			if (isLocal) obj.transform.localPosition = pos;
			else obj.transform.localScale = Vector3.one;
			return obj;
		}

		static public void Coin(UILabel label, decimal coin)
		{
			if (string.IsNullOrEmpty(label.text))
			{
				label.gameObject.name = coin.ToString();
				label.text = BossUtil.DecimalToPromise(coin);
			}

			if (coin == decimal.Parse(label.gameObject.name))
			{ 
				Pung(label.gameObject);
				return;
			}

			if (coinAnimFin == false)
			{
				CoroutineManager.Instance.StopCoroutine(CoinAnimation(label, coin));
			}
			coinAnimFin = false;
			CoroutineManager.Instance.StartCoroutine(CoinAnimation(label, coin));
		}
		static IEnumerator CoinAnimation(UILabel label, decimal coin)
		{
			float endTime = Time.time + coinAnimTime;
			decimal origin = decimal.Parse(label.gameObject.name);
			decimal add = coin - origin;
			while (true)
			{
				float now = Time.time;
				if (now >= endTime) break;
				float per = (endTime - now) / coinAnimTime;
				per = Mathf.Clamp(per, 0.0f, 1.0f);
				decimal temp = origin + (add * (decimal)(1.0f - per));
				temp = decimal.Round(temp);
				label.gameObject.name = temp.ToString();
				label.text = BossUtil.DecimalToPromise(temp);
				yield return null;
			}

			label.gameObject.name = coin.ToString();
			label.text = BossUtil.DecimalToPromise(coin);
			coinAnimFin = true;
			yield break;
		}
	}

	static public class Player
	{
		static public void Set(string key, string value)
		{
			PlayerPrefs.SetString(key, value);
		}

		static public string Get(string key, string defaultValue)
		{
			string value = PlayerPrefs.GetString(key, defaultValue);
			if (value == defaultValue) Set(key, defaultValue);
			return value;
		}

		static public void SetInt(string key, int value)
		{
			PlayerPrefs.SetInt(key, value);
		}

		static public int GetInt(string key, int defaultValue)
		{
			int value = PlayerPrefs.GetInt(key, defaultValue);
			if (value == defaultValue) SetInt(key, defaultValue);
			return value;
		}
	}

	static public T XmlToClass<T>(string path)
		where T : class, new()
	{
		T t = new T();
		XmlSerializer serializer = new XmlSerializer(typeof(T));
		TextAsset textAsset = Resources.Load(Defines.XML_PATH+path) as TextAsset; 
		if (textAsset == null)
		{
			BossDebug.LogError("파일을 찾을 수 없어요\nResources/"+Defines.XML_PATH+path);
			return null;
		}
		MemoryStream stream = new MemoryStream(textAsset.bytes); 
		XmlReader xmlReader = XmlReader.Create(stream); 
		t = serializer.Deserialize(xmlReader) as T;
		xmlReader.Close();
		stream.Close();
		return t;
	}

	static public decimal Add(decimal sour, decimal dest)
	{
		decimal value;

		try {
			value = sour + dest;
		} catch (OverflowException e) {
			BossDebug.LogWarning("이런"+e);
			value = decimal.MaxValue;
		}
		return value;
	}

	static public string DecimalToPromise(decimal value)
	{
		string promise = " kMBTqQsSONdUD!@#$%^&";
		string toString = value.ToString();
		int length = toString.Length;
		return (toString.Substring(0, (length % 3) == 0 ? 3 : (length % 3))) +
			(length > 3 ? promise[(length-1) / 3].ToString() : "");
	}
}
