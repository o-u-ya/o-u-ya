﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item
{
	public class ConvertItem
	{
		public int	   id;
        public int     itemgroup;
        public int     itemlevel;
        public string  name;
        public string  iteminfo;
		public decimal price;
		public decimal damage;
		public decimal total;
		public string  image;

		public ConvertItem(ItemDataItem itemDataItem)
		{
			this.id = int.Parse(itemDataItem.id);
            this.itemgroup = int.Parse(itemDataItem.itemgroup);
            this.itemlevel = int.Parse(itemDataItem.itemlevel);
            this.name = itemDataItem.name;
            this.iteminfo = itemDataItem.iteminfomation;
			this.price = decimal.Parse(itemDataItem.price);
			this.damage = decimal.Parse(itemDataItem.damage);
			this.total = decimal.Parse(itemDataItem.total);
			this.image = itemDataItem.image;
		}
	}

	private ItemData m_ItemData;
    private int m_MinIndex;
    public int MinIndex
    {
        get { return m_MinIndex; }
        set { m_MinIndex = value; }
    }

    private int m_MaxIndex;
    public int MaxIndex
    {
        get { return m_MaxIndex; }
        set { m_MaxIndex = value; }
    }

	private List<ConvertItem> m_ConvertItemList = new List<ConvertItem>();
	public List<ConvertItem> ConvertItemList
	{
		get { return m_ConvertItemList; }
		set { m_ConvertItemList = value; }
	}

	public void Load()
	{
		m_ItemData = BossUtil.XmlToClass<ItemData>("ItemData");
        GetItemList();
	}

    public void GetItemList()
    {
        List<ConvertItem> pList = new List<ConvertItem>();

        for (int nCnt = 0; nCnt < m_ItemData.Items.Length; nCnt++ )
        {
            pList.Add(GetItem(nCnt));
        }

		ConvertItemList = pList;
    }

	public int GetGroupItemList(bool First ,int GroupNumber)
    {
		int FirstID = 0;
		int LastID = 0;
        for (int nCnt = 0; nCnt < ConvertItemList.Count; nCnt++)
        {
            if (ConvertItemList[nCnt].itemgroup == GroupNumber)
            {
				if(First)
				{
					FirstID = ConvertItemList[nCnt].id;
					break;
				}
				else LastID = ConvertItemList[nCnt].id;
            }
        }

        return LastID;
    }

    public ConvertItem GetItem(int itemid)
    {
        ConvertItem item = new ConvertItem(m_ItemData.Items[itemid]);
        
        return item;
    }

    /// <summary>
    /// Min = True, Max = false
    /// </summary>
    /// <param name="TMin_FMax"></param>
    /// <param name="GroupNumber"></param>
    /// <returns></returns>
    public int MinID_MaxID(bool TMin_FMax = false, int GroupNumber = 1)
    {
		return GetGroupItemList(TMin_FMax,GroupNumber);
    }

    //public ConvertItem GetItem(int id, int level)
    //{
    //    if (id != Mathf.Clamp(id, 0, m_ItemData.Items.Length))
    //    {
    //        BossDebug.LogError("ItemData의 id : " + id + "의 내용을 확인 할 수 없어요");
    //        return null;
    //    }
    //    else if (level < 0) return null;

    //    ConvertItem Item = new ConvertItem(m_ItemData.Items[id]);
    //    decimal originPrice = Item.price;
    //    Item.damage *= level;
    //    for (int i = 1; i < level; i++)
    //    {
    //        Item.price += originPrice * (Item.price_amp + Item.price_plus * i);
    //    }
    //    return Item;
    //}

}