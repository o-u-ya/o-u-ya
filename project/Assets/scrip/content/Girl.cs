﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/*
 * 시작, 종료 단계에 따라 몹 선택
 */
public class Girl
{
	private enum Hole : int {
		_Size,
		_x,
		_y
	};

	private const int			maskLevelMax = 99;
	private const int			holeCount = 4;
	private const string		maskProp = "_Alpha";
	private Hero				m_Hero;
	private bossLoading			m_BossLoading;
	private BossData			bossData;
	private BossResourceData	bossResourceData;
	private Texture2D[,]		mask;
	private UITexture			body;
	private UIPanel				boardPanel;
	private List<UITexture>		clothes = new List<UITexture>();
	private int					playStep;
	private int					maskStep;
	private int					maskLevel;
	private int					CurrentMaskLevel;


	public void SetUp(Hero hero, UITexture body, UIPanel boardPanel)
	{
		m_Hero = hero;
		this.body = body;
		this.boardPanel = boardPanel;
	}

	public void Load()
	{
		bossData = BossUtil.XmlToClass<BossData>("BossData");
		bossResourceData = BossUtil.XmlToClass<BossResourceData>("BossResourceData");
	}

	public void SetHole(float size)
	{
		int level = (int)(maskLevelMax * size);
		if (playStep != Mathf.Clamp(playStep, 0, clothes.Count - 1) ||
			level != Mathf.Clamp(level, 0, mask.Length - 1)) return;
		maskLevel = level;
		CoroutineManager.Instance.StopCoroutine("SetHoleAnim");
		CoroutineManager.Instance.StartCoroutine(SetHoleAnim());
		//clothes[playStep].material.SetTexture(maskProp, mask[maskStep, level]);
		//boardPanel.Refresh();
	}

	IEnumerator SetHoleAnim()
	{
		while (maskLevel != CurrentMaskLevel)
		{
			CurrentMaskLevel++;
			clothes[playStep].material.SetTexture(maskProp, mask[maskStep, CurrentMaskLevel]);
			boardPanel.Refresh();
			yield return null;
		}

		yield break;
	}

	public void Undress() {
        //int clothesId = clothes.Count - 1 - playStep;
        //if (clothesId != Mathf.Clamp(clothesId, 0, clothes.Count)) return;
        //NGUITools.SetActive(clothes[ clothesId ].gameObject, false);
        //playStep++;

        NGUITools.SetActive(clothes[playStep].gameObject, false);
		CurrentMaskLevel = maskLevel = 0;
        playStep++;
		maskStep--;
	}

	public void Reset()
	{
		for (int i=0 ; i<= playStep ; i++)
		{
			clothes[i].material.SetTexture(maskProp, mask[mask.GetLength(0) - 1 - i, 0]);
			NGUITools.SetActive(clothes[i].gameObject, true);
		}
		boardPanel.Refresh();
		maskStep = mask.GetLength(0) - 1;
		playStep = CurrentMaskLevel = maskLevel = 0;
	}

	public void Set(int level, int step, bool boss)
	{
		if (body == null) return;
		NGUITools.SetActive(body.gameObject, boss);
		if (!boss) return;
		m_Hero.Lock(true);

		CurrentMaskLevel = maskLevel = 0;
		m_BossLoading = PageManager.Instance.Open(ePageType._BossLoading).GetComponent<bossLoading>();
        m_BossLoading.SetUp(m_Hero);
		CoroutineManager.Instance.StartCoroutine(Load(level, step));
	}

	IEnumerator Load(int level, int step)
	{
		playStep = 0;
		int bossLevel = ((level / (Defines.MONSTER_STEP+1)) - 1) % bossData.Items.Length;
		int startStep = int.Parse(bossData.Items[bossLevel].start);
		int endStep = int.Parse(bossData.Items[bossLevel].end);
		
		yield return CoroutineManager.Instance.StartCoroutine(
			LoadMask(
				bossData.Items[bossLevel].resource,
				startStep,
				endStep
			)
		);
		yield return CoroutineManager.Instance.StartCoroutine(
			LoadBodyClothes(
				bossLevel,
				startStep,
				endStep,
				step
			)
		);

        for (int i=0 ; i<step ; i++) Undress();

		yield break;
	}

	IEnumerator LoadBodyClothes(int bossLevel, int startStep, int endStep, int step)
	{
		//이전옷 정리
		foreach (UITexture This in clothes) NGUITools.DestroyImmediate(This.gameObject);
		clothes.Clear();

		//새몸통, 옷 불러오기
		SetTexture(body, bossData.Items[bossLevel].resource);
		GameObject clothesPrefabs = Resources.Load(Defines.MONSTER_PREFABS_PATH) as GameObject;
		Material material = Resources.Load(Defines.MONSTER_MATERIAL_PATH) as Material;
        for (int i = startStep; i >= 0; i--)
        {
            yield return null;
            GameObject obj = NGUITools.AddChild(body.gameObject, clothesPrefabs);
            obj.name = i.ToString();

            int clothesId = clothes.Count;
            clothes.Add(obj.GetComponent<UITexture>());
            clothes[clothesId].depth = i + 1;
            Material mat = GameObject.Instantiate(material) as Material;
            mat.mainTexture = Resources.Load(Defines.BOARD_PATH + bossData.Items[bossLevel].resource + "_" + i.ToString()) as Texture;
            clothes[clothesId].material = mat;

            if (i >= endStep)
            {
                clothes[clothesId].material.SetTexture(maskProp, mask[clothesId, 0]);
            }
        }

		yield break;
	}

	IEnumerator LoadMask(string resourceKey, int startStep, int endStep)
	{
		int useStep = startStep - endStep + 1;
		int maskSize = Defines.MASKSIZEX * Defines.MASKSIZEY;
		Color32[]	white;
		Color32[]	rgb;


		maskStep = useStep - 1;
		white = new Color32[maskSize];
		for (int x=0 ; x<Defines.MASKSIZEX ; x++)
		{
			for (int y=0 ; y<Defines.MASKSIZEY ; y++)
			{
				white[Defines.MASKSIZEX * y + x] = new Color32(0xFF, 0xFF, 0xFF, 0xFF);
			}
		}

		if (mask != null)
		{
			for (int i=0 ; i<mask.GetLength(0) ; i++)
			{
				for (int j=0 ; j<mask.GetLength(1) ; j++)
				{
					UnityEngine.Object.Destroy(mask[i,j]);
				}
			}
		}
		mask = new Texture2D[ useStep,maskLevelMax ];
		for (int step = 0 ; step<useStep ; step++)
		{
			int resourceId = FindResourceIdFromKeyAndLayer(resourceKey, endStep + step);
			for (int i=0 ; i<maskLevelMax ; i++)
			{
				yield return null;
				mask[step,i] = new Texture2D(Defines.MASKSIZEX, Defines.MASKSIZEY, TextureFormat.ARGB32, true, true); 
				rgb = new Color32[maskSize];
				Array.Copy(white, rgb, maskSize);

				string[,] holeList = {
					{
						bossResourceData.Items[resourceId].hole_0_size,
						bossResourceData.Items[resourceId].hole_0_x,
						bossResourceData.Items[resourceId].hole_0_y
					},
					{
						bossResourceData.Items[resourceId].hole_1_size,
						bossResourceData.Items[resourceId].hole_1_x,
						bossResourceData.Items[resourceId].hole_1_y
					},
					{
						bossResourceData.Items[resourceId].hole_2_size,
						bossResourceData.Items[resourceId].hole_2_x,
						bossResourceData.Items[resourceId].hole_2_y
					},
					{
						bossResourceData.Items[resourceId].hole_3_size,
						bossResourceData.Items[resourceId].hole_3_x,
						bossResourceData.Items[resourceId].hole_3_y
					}
				};
				for (int hole = 0 ; hole < holeCount ; hole++)
				{
					if (holeList[hole,(int)Hole._Size].Equals(Defines.None))
					{
						continue;
					}

					float size = Defines.MASKSIZE * float.Parse(holeList[hole,(int)Hole._Size]);
					float x = Defines.MASKSIZEX * float.Parse(holeList[hole,(int)Hole._x]);
					float y = Defines.MASKSIZEY * float.Parse(holeList[hole,(int)Hole._y]);

					MakeHole(ref rgb, (int)x, (int)y, (int)(size * (i / (float)maskLevelMax)));
				}

				mask[step,i].SetPixels32(rgb);
				mask[step,i].Apply();
			}
		}
		m_BossLoading.Complete();
		yield break;
	}

	private void MakeHole(ref Color32[] mask, int x, int y, int size)
	{
		if (size <= 0) return;

		for (int r=1 ; r<size ; r++)
		{
			for (float radius = 0.0f ; radius < (Mathf.PI * 2) ; radius = (radius+Mathf.PI / 8 /r))
			{
				int posX = x + (int)(r * Mathf.Cos(radius));
				int posY = (y + (int)(r * Mathf.Sin(radius)));
				if (posX != Mathf.Clamp(posX, 0, Defines.MASKSIZEX - 1) ||
					posY != Mathf.Clamp(posY, 0, Defines.MASKSIZEY - 1))
				{
					continue;
				}

				int temp = Defines.MASKSIZEX * posY + posX;
				if (temp != Mathf.Clamp(temp, 0, mask.Length - 1))
				{
					BossDebug.LogWarning("ang? x:"+posX+", y:"+posY);
					continue;
				}
				mask[temp] = new Color32(0xFF, 0xFF, 0xFF, 0);
			}
		}
	}

	//
	// 텍스쳐 셋힝
	//
	private void SetTexture(UITexture Texture, string path)
	{
		if ((Texture.mainTexture == null) ||
			(Texture.mainTexture.Equals(path) == false))
		{
			Texture.mainTexture = Resources.Load(Defines.BOARD_PATH+path) as Texture;
		}
	}

	private int FindResourceIdFromKeyAndLayer(string key, int layer) 
	{
		for (int i=0 ; i<bossResourceData.Items.Length ; i++)
		{
			if (string.IsNullOrEmpty(bossResourceData.Items[i].key))
			{
				BossDebug.LogWarning("BossResourceData.xml에 key가 비어있는 아이템이 있어요");
				continue;
			}
			if (bossResourceData.Items[i].key.Equals(key) &&
				int.Parse(bossResourceData.Items[i].layer) == layer) return i;
		}

		BossDebug.LogWarning("아이템을 찾지 못했어요 key:"+key+", layer:"+layer);
		return -1;
	}

	public BossDataItem FindBossDataFromLevel(int level) {
        // ((level / (Defines.MONSTER_STEP+1)) - 1) % bossData.Items.Length;
        return bossData.Items[((level / (Defines.MONSTER_STEP + 1)) - 1) % bossData.Items.Length];
	}

	//private BossResourceDataItem FindResourceFromKeyAndLayer(string key, int layer) 
	//{
	//	for (int i=0 ; i<bossResourceData.Items.Length ; i++)
	//	{
	//		if (string.IsNullOrEmpty(bossResourceData.Items[i].key))
	//		{
	//			BossDebug.LogWarning("BossResourceData.xml에 key가 비어있는 아이템이 있어요");
	//			continue;
	//		}
	//		if (bossResourceData.Items[i].key.Equals(key) &&
	//			int.Parse(bossResourceData.Items[i].layer) == layer) return bossResourceData.Items[i];
	//	}

	//	BossDebug.LogWarning("아이템을 찾지 못했어요 key:"+key+", layer:"+layer);
	//	return null;
	//}
}