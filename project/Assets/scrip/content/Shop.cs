﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Shop : MonoBehaviour
{
    public enum eTapState
    {
        eTS_NONE,
        eTS_Skill,
        eTS_InApp,
    }

    [SerializeField]
    private GameObject Tap_Skill;
    [SerializeField]
    private GameObject Tap_InApp;

    eTapState m_eTapState = eTapState.eTS_NONE;

    void Start()
    {
        AddEvent();
    }

    void AddEvent()
    {
        BossUtil.Button.AddEvent(Tap_Skill, OnClick_Skill);
        BossUtil.Button.AddEvent(Tap_Skill, OnClick_InApp);
    }

    void OnClick_Skill(GameObject o)
    {
        m_eTapState = eTapState.eTS_Skill;
    }

    void OnClick_InApp(GameObject o)
    {
        m_eTapState = eTapState.eTS_InApp;
    }

}

