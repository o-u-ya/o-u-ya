﻿#define LEVEL

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Monster
{
	private enum AnimState 
	{
		_stand,
		_hit,
		_die,
		_end
	};

	private Stage				m_Stage;
	private Hero				m_Hero;
	private UISprite			obj;
	private readonly string[]	promise = {"stand", "hit", "die"}; //AnimState
	private MonsterAnimData		monsterAnimData;
    //private BossData			bossData;
//#if LEVEL
//    private MonsterLevelData	monsterLevelData;
//#endif
    //private int					level;
    //private int					step;
	private int					monsterId = -1;
	private int					frame;
	private float				time;
    //private decimal				hp;			//현재 체력
    //private decimal				hpMax;		//최대 체력
    //private decimal				coin;		//처치시 획득량
	private AnimState			animState;
	private float				die;			//죽은시간
	//private List<UITexture>		monster = new List<UITexture>();

	public void SetUp(Stage stage, Hero hero, UISprite obj)
	{
		m_Stage = stage;
		m_Hero = hero;
		this.obj = obj;
		monsterAnimData = BossUtil.XmlToClass<MonsterAnimData>("MonsterAnimData");
        //bossData = BossUtil.XmlToClass<BossData>("BossData");
//#if LEVEL
//        monsterLevelData = BossUtil.XmlToClass<MonsterLevelData>("MonsterLevelData");
//#endif
	}

	public void Set(int level, int step, bool boss)
	{
		if (boss) {obj.enabled = false; return;}
		else obj.enabled = true;

//        this.level = level;
//        this.step = step;
//#if LEVEL
//        level = Mathf.Clamp(level, 0, 99);
//        hpMax = hp = decimal.Parse(monsterLevelData.Items[level].hp);
//        coin = decimal.Parse(monsterLevelData.Items[level].coin);
//#else
//        hpMax = hp = GlobalDataManager.Instance.GetItem(GlobalDataManager.keyStageHp, level).Value;
//        coin = GlobalDataManager.Instance.GetItem(GlobalDataManager.keyStageCoin, level).Value;
//#endif
		//int[] hello = new int[10];
		//for (int i=0 ; i<100 ; i++)
		//{
		//	hello[Random.Range(0, monsterAnimData.Items.Length)]++;
		//}

		//foreach (int This in hello)
		//{
		//	Debug.Log(This);
		//}
		 
		while (true) 
		{
			if (monsterAnimData.Items.Length <= 1) {monsterId = 0; break;}
			int id = Random.Range(0, monsterAnimData.Items.Length);
			if (id != monsterId)
			{
				monsterId = id;
				break;
			}
		}
		Start();



		//for (int i=0 ; i<monster.Count ; i++)
		//{
		//	NGUITools.DestroyImmediate(monster[i].gameObject);
		//}
		//monster.Clear();

		//if (boss)
		//{
		//}

		//else
		//{


		//	//GameObject monsterPrefabs = Resources.Load(Defines.MONSTER_PATH) as GameObject;
		//	//for (int i=0 ; i<monster.Count ; i++)
		//	//{
		//	//	NGUITools.DestroyImmediate(monster[i].gameObject);
		//	//}
		//	//monster.Clear();
		//	//GameObject obj = NGUITools.AddChild(parent, monsterPrefabs);
		//	//obj.name = "0";
		//	//monster.Add(obj.GetComponent<UITexture>());

		//}
	}

	private void Start(AnimState state = AnimState._stand)
	{
		animState = state;
		time = float.MaxValue;
		frame = -1;
		obj.color = Color.white;
		Management();
	}

	public void Management()
	{
		if (obj.enabled == false) return;

		AnimState	state;
		int			max;

		time += Time.deltaTime;
		if (time > 0.15f)
		{
			time = 0.0f;

			frame++;
			state = animState;
			if ((animState == AnimState._die) &&
				(int.Parse(monsterAnimData.Items[monsterId].die) == 0))
			{
				state = AnimState._hit;
			}
			switch (state)
			{
				case AnimState._hit:
					max = int.Parse(monsterAnimData.Items[monsterId].hit);
					break;
				case AnimState._die:
					max = int.Parse(monsterAnimData.Items[monsterId].die);
					break;
				default:
					max = int.Parse(monsterAnimData.Items[monsterId].stand);
					break;
			}
			if (frame >= max)
			{
				if ((animState != AnimState._die) && (state == AnimState._hit))
					state = animState = AnimState._stand;
				frame = ((animState == AnimState._die) ? (max - 1) : 0);
			}

			obj.spriteName = monsterAnimData.Items[monsterId].resource + "_" +
				promise[(int)state] + "_" + frame.ToString();
			obj.width = int.Parse(monsterAnimData.Items[monsterId].width);
			obj.height = int.Parse(monsterAnimData.Items[monsterId].height);
		}

		if (animState == AnimState._die)
		{
			float per = Mathf.Clamp((Time.time - die) / Defines.TIME_DIE, 0.0f, 1.0f);
			obj.color = new Color(1.0f, 1.0f, 1.0f, 1.0f - per);
			if (per == 1.0f)
			{
				m_Hero.LockOnly(false);
				m_Stage.NextStep();
			}
		}
	}


	public void Hit()
	{
		if (animState == AnimState._hit || animState == AnimState._die) return;
		Start(AnimState._hit);
	}

	public void Die()
	{
		die = Time.time;
		Start(AnimState._die);
	}
}