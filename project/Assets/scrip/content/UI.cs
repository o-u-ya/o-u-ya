﻿#define RESET

using UnityEngine;
using System;
using System.Collections;


[Serializable]
public class UI
{
	[Serializable]
    public abstract class State
    {
        [SerializeField] private GameObject	obj;
        [SerializeField] protected UISprite	bar;

		//private GameObject	touchEffect;
		protected decimal	maxHp;
		protected float		hpp;

		public void SetActive(bool active)
        {
            NGUITools.SetActive(obj, active);
        }

		virtual public void SetUp(decimal maxHp, int step, float? timeOver, bool leave)
		{
			this.maxHp = maxHp;
			SetHP(maxHp);
		}

		//public void LoadEffect(string path)
		//{
		//}

		virtual public void SetHP(decimal hp)
		{
			hpp = (float)(hp / maxHp);
		}
    }

    [Serializable]
    public class Boss : State
    {
		private Stage m_Stage;
        [SerializeField] protected UISprite	timer;
		[SerializeField] private GameObject	sorry;
		private float timeOver;
		private float start;
		private bool run = false;
		
		public void SetUp(Stage stage)
		{
			m_Stage = stage;
			BossUtil.Button.AddEvent(sorry, OnClickSorry);
		}

		public void SetSorryActive(bool active)
		{
			NGUITools.SetActive(sorry, active);
		}

		private void OnClickSorry(GameObject obj)
		{
			m_Stage.Sorry();
		}

		public override void SetUp(decimal maxHp, int step, float? timeOver, bool leave)
		{
			base.SetUp(maxHp, step, timeOver, leave);
			timer.fillAmount = 0.0f;
			this.timeOver = timeOver.Value;
		}

		public override void SetHP(decimal hp)
		{
			base.SetHP(hp);
			bar.fillAmount = 1.0f - hpp;
		}

		public void SetTimer(float per)
		{
			timer.fillAmount = per;
		}

		//public void StartTimer()
		//{
		//	start = Time.time;
		//	if (run == false) CoroutineManager.Instance.StartCoroutine(TimeOver(timeOver));
		//}

		//IEnumerator TimeOver(float time)
		//{
		//	run = true;
		//	while (true)
		//	{
		//		float per = (Time.time - start) / time;
		//		per = Mathf.Clamp(per, 0.0f, 1.0f);
		//		timer.fillAmount = per;
		//		if (per == 1.0f) break;
		//		yield return null;
		//	}
		//	run = false;
		//	yield break;
		//}
    }

    [Serializable]
    public class Monster : State
    {
		private Stage m_Stage;
	    [SerializeField] private UILabel    point;
		[SerializeField] private UILabel	step;
		[SerializeField] private GameObject	fight;

		public void SetFightActive(bool active)
		{
			NGUITools.SetActive(fight, active);
		}


		public void SetUp(Stage stage)
		{
			m_Stage = stage;
			BossUtil.Button.AddEvent(fight, OnClickFight);
		}

		private void OnClickFight(GameObject obj)
		{
			m_Stage.Fight();
		}

		public override void SetUp(decimal maxHp, int step, float? timeOver, bool leave)
		{
			base.SetUp(maxHp, step, timeOver, leave);

			if (step != 0) {
				this.step.gameObject.SetActive(true);

				NGUITools.SetActive(this.step.gameObject, !leave);
				NGUITools.SetActive(this.fight, leave);
				if (leave == false) {
					this.step.text = ((step % (Defines.MONSTER_STEP + 1)) - 1) + "/" + Defines.MONSTER_STEP;
				}
			//return (level != 0) && ((level % (Defines.MONSTER_STEP + 1)) == 0);
			}
			else this.step.gameObject.SetActive(false);
		}

		public override void SetHP(decimal hp)
		{
			base.SetHP(hp);
			bar.fillAmount = hpp;
			point.text = BossUtil.DecimalToPromise(hp);
		}
    }

    [SerializeField] private Boss			boss = new Boss();
    [SerializeField] private Monster		monster = new Monster();
	[SerializeField] private GameObject		coin;
	[SerializeField] private UILabel		coinPoint;
	[SerializeField] private GameObject		autoLeave;
	[SerializeField] private UISprite		autoLeaveMark;
#if RESET
	[SerializeField] private GameObject		reset;

	private void OnClickReset(GameObject obj)
	{
		Message.Instance.Show("정말?", delegate {
			NetworkManager.Instance.Send(
				"login",
				iTween.Hash(
					"do", "reset",
					"uuid", SystemInfo.deviceUniqueIdentifier.Substring(0, 32)),
				delegate (string result, SimpleJSON.JSONNode data) {
					if (string.Compare(result, NetworkManager.RESULT_SUCCESS) == 0) {
						Application.LoadLevel("restart");
					}
				}
			);
		});

				
	}

#endif 

	public enum Type
	{
		_Boss,
		_Monster,
		_End
	}

	private Type			m_eType = Type._End;
	private State			m_State;
	private Stage			m_Stage;


	public void SetUp(Stage stage)
	{
		m_Stage = stage;
		boss.SetUp(m_Stage);
		monster.SetUp(m_Stage);

#if RESET
		BossUtil.Button.AddEvent(reset, OnClickReset);
#else
		NGUITools.SetActive(reset, false);
#endif
	}

	public void SetState(Type type)
	{
		if (m_eType == type) return;
		m_eType = type;
		if (Get() != null) Get().SetActive(false);

		switch (type)
		{
			case Type._Boss:       m_State = boss;         break;
			case Type._Monster:    m_State = monster;      break;
			default:
				m_State = null;
				BossDebug.LogError("ang? state:"+type);
				return;
		}

		Get().SetActive(true);
	}

	public State Get() {return m_State;}
	public Boss GetBoss() {return (m_State as Boss);}

	//public void StartTimer()
	//{
	//	(m_State as Boss).StartTimer();
	//}

	public void SetCoin(decimal coin)
	{
		BossUtil.Animation.Coin(this.coinPoint, coin);
		BossUtil.Animation.Pung(this.coin);
	}
	public void EnterBossLoading()
	{
		// 1. 코인창 숨김
		NGUITools.SetActive(this.coin, false);
		// 2. 싸우자, 미안 숨김
		boss.SetSorryActive(false);
		monster.SetFightActive(false);
	}
	public void BossLoadingComplete()
	{
		// 1. 코인창 보이기
		NGUITools.SetActive(this.coin, true);
		// 2. 싸우자, 미안 보이기
		boss.SetSorryActive(true);
		monster.SetFightActive(true);
	}
}
