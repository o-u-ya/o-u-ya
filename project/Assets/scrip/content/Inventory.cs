﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class Inventory : Boss.MonoSingleton<Inventory> 
{
	private UI			m_UI;
	private Hero		m_Hero;
	private Item		m_Item;
	private Dictionary<int,decimal> hire = new Dictionary<int,decimal>();

    public delegate void GetInventoryEvent();
    public GetInventoryEvent GetInventoryEventHandler;
    public delegate void UpdateInventoryItemEvent(int itemgroup, int itemid);
    public UpdateInventoryItemEvent UpdateInventoryItemEventHandler;

	private const string key_coin =	"boss_inven_coin";

	private decimal coin;
	public decimal Coin {get {return coin;}}

    private List<InventoryData> m_InventoryDataList = new List<InventoryData>();
    public List<InventoryData> InventoryDataList
    {
        get { return m_InventoryDataList; }
        set { m_InventoryDataList = value; }
    }

	public void Load()
	{
		coin = decimal.Parse(BossUtil.Player.Get(key_coin, "0"));
	}

	public void SetUp(UI ui, Hero hero, Item item)
	{
		m_UI = ui;
		m_Hero = hero;
		m_Item = item;

		m_UI.SetState(UI.Type._Monster);
		m_UI.SetCoin(this.coin);
	}

	static public void Save(decimal coin)
	{
		BossUtil.Player.Set(key_coin, coin.ToString());
	}

	public void AddCoin(decimal coin)
	{
		this.coin = BossUtil.Add(this.coin, coin);
		m_UI.SetCoin(this.coin);
		Save(this.coin);
	}

    public void GetInventory(int id)
    {
        NetworkManager.Instance.Send(
        "inventory",
        iTween.Hash("do", "inventory_get", "id", id),
        delegate(string result, SimpleJSON.JSONNode data)
        {
            string json = data.ToString();
            JsonData ConvertData = JsonMapper.ToObject(json);
            for (int nCnt = 0; nCnt < ConvertData.Count; nCnt++)
            {
                string szConvert = ConvertData[nCnt].ToJson();
                InventoryData pInventoryData = JsonMapper.ToObject<InventoryData>(szConvert);
                Inventory.Instance.InventoryDataList.Add(pInventoryData);
                Inventory.Instance.InventoryDataList.Sort(SortByItemGroup);
            }
            if(GetInventoryEventHandler != null)
            {
                GetInventoryEventHandler();
            }

			// 데미지 계산
			foreach (InventoryData This in Inventory.Instance.InventoryDataList)
			{
				int itemId = int.Parse(This.itemid) - 1;

				if (int.Parse(This.itemgroup) != 1)
				{
					int itemGroup = int.Parse(This.itemgroup);

					if (hire.ContainsKey(itemGroup)) 
					{
						BossDebug.LogError("어째서 같은 아이템이");
						continue;
					}

					hire.Add
					(
						itemGroup,
						m_Item.GetItem(itemId).total
					);
				}

				else
				{
					m_Hero.TabDmg = m_Item.GetItem(itemId).total;
				}
			}

			foreach (KeyValuePair<int,decimal> This in hire)
			{
				m_Hero.HireDmg += This.Value;
			}

        },
        delegate(string err)
        {
            PageManager.Instance.Open(ePageType._Game);
        });
    }

    public int SortByItemGroup(InventoryData x, InventoryData y)
    {
        return x.itemgroup.CompareTo(y.itemgroup);
    }

    public void UpdateInventoryItem(int itemgroup, int itemid)
    {
        if(UpdateInventoryItemEventHandler != null)
        {
            UpdateInventoryItemEventHandler(itemgroup, itemid);
        }

		// 데미지 업데이트
		if (itemgroup != 1)
		{
			if (hire.ContainsKey(itemgroup))
			{
				decimal dmg = m_Item.GetItem(itemid).total;
				decimal add = dmg - hire[itemgroup];
				hire[itemgroup] = dmg;
				m_Hero.HireDmg += add;
			}
			else
			{
				decimal dmg = m_Item.GetItem(itemid).total;
				hire.Add(itemgroup, dmg);
				m_Hero.HireDmg += dmg;
			}
		}

		else
		{
			m_Hero.TabDmg = m_Item.GetItem(itemid - 1).total;
		}
    }

}