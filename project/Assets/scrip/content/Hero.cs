﻿using UnityEngine;
using System.Collections;

public class Hero
{
	private Stage		m_Stage;
	private Item		m_Item;
	//private Inventory	m_Inven = new Inventory();
	private UI			m_UI;
	private bool		m_Lock = false;

	private decimal tabDmg;

	public decimal TabDmg
	{
		get { return tabDmg; }
		set { tabDmg = value; }
	}
	private decimal hireDmg;

	public decimal HireDmg
	{
		get { return hireDmg; }
		set { hireDmg = value; }
	}

	private GameObject	dropCoinEffect;
	private GameObject	tapDamage;
	private GameObject	tapEffect;

	public void Load()
	{
        Inventory.Instance.Load();
		dropCoinEffect = Resources.Load(Defines.PARTICLE_PATH+"dropCoin") as GameObject;
		tapDamage = Resources.Load(Defines.PARTICLE_PATH+"tapDamage") as GameObject;
		tapEffect = Resources.Load(Defines.PARTICLE_PATH+"attack") as GameObject;
	}

	public void LockOnly(bool state)
	{
		m_Lock = state;
	}

	public void Lock(bool state)
	{ 
		LockOnly(state);

		if (m_Lock == false)
		{
			//m_UI.StartTimer();
			m_Stage.Count();
			m_UI.BossLoadingComplete();
		}
		else {
			m_UI.EnterBossLoading();
		}
	}

	public void SetUp(Stage stage, Item item, UI ui)
	{
		m_Stage = stage;
		m_Item = item;
		m_UI = ui;
		Inventory.Instance.SetUp(m_UI, this, m_Item);
		//m_UI.SetState(UI.Type._Monster);
		//m_UI.Get().SetCoin(Inventory.Instance.Coin);
	}

	public string buf;
	public void Management()
	{
		if (m_Lock == true) return;

#if UNITY_EDITOR || UNITY_STANDALONE
		if (Input.GetButtonDown("Fire1"))
		{
			//Ray ray = PageManager.Instance.Parent.camera.ScreenPointToRay(Input.mousePosition);
			//RaycastHit hit;
 
			//if (Physics.Raycast(ray, out hit))
			//{
			//	Debug.Log(hit.collider.tag+" : "+hit.distance);
			//}
			//Input.GetPosition

			Attack(PageManager.Instance.Parent.camera.ScreenToWorldPoint(Input.mousePosition));
		}
#else
		Touch[] touches = Input.touches;
		foreach (Touch This in touches)
		{
			if (This.phase == TouchPhase.Began)
			{
				Attack(PageManager.Instance.Parent.camera.ScreenToWorldPoint(This.position));
			}
		}
#endif

		//자동 공격
		point += HireDmg * (decimal)Time.deltaTime;
		decimal floor = decimal.Floor(point);
		if (floor > 1)
		{
			point -= floor;
			m_Stage.Attack(floor, true);
		}
		
	}
	decimal point = 0;

	private void Attack(Vector2 pos)
	{
		//Vector2 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Ray2D ray = new Ray2D (pos, Vector2.zero);
		RaycastHit2D[] hit = Physics2D.RaycastAll(ray.origin, ray.direction);
		decimal tabDmg = 1 + TabDmg;

		if (hit.Length == 1 && 
			string.IsNullOrEmpty(hit[0].collider.gameObject.tag) == false &&
			hit[0].collider.gameObject.tag.Equals("Monster"))
		{
			BossUtil.Animation.Particle(pos, tapEffect, false);
			BossUtil.Animation.Particle(Vector3.zero, tapDamage, false).GetComponent<TabDamage>().Set(tabDmg);

			m_Stage.Attack(tabDmg);
		}
	}

	public void AddCoin(decimal coin)
	{
        Inventory.Instance.AddCoin(coin);
		//m_Stage.SetCoin(Inventory.Instance.Coin);
		BossUtil.Animation.Particle(new Vector3(0.0f, -300.0f), dropCoinEffect);
	}

    public decimal GetCoin() { return Inventory.Instance.Coin; }
}
