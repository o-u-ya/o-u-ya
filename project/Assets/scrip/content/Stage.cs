﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

public class Stage
{ 
	private const string key_level =	"boss_stage_level";
	private const string key_step =		"boss_stage_step";
	private const string key_leave =	"boss_stage_leave";

	private Hero			m_Hero;
	private Back			m_Back;
	private Monster			m_Monster;
	private Girl			m_Girl;
	//private UITexture		girls;
	//private List<UITexture>	monster = new List<UITexture>();
    private UI              m_UI;
	private UIPanel			boardPanel;


	private class PlayStage
	{
		public int		level;
		public int		thisStep;
		public int		step;
		public decimal	thisHp;
		public decimal	hp;
		public decimal	coin;
		public float?	timeOver;
		public bool		isBoss;
		public bool		leave;

		public PlayStage()
		{
		}

		public PlayStage(Girl girl, StageData stageData)
		{
			level = stageData.level;
			step = stageData.step;
			hp = stageData.hp;
			coin = stageData.coin;
			isBoss = IsBoss(level);

			timeOver = null;
			if (isBoss) {
				BossDataItem bossDataItem = girl.FindBossDataFromLevel(level);
				int start = int.Parse(bossDataItem.start);
				int end = int.Parse(bossDataItem.end);
				step = start - end + 1;
				timeOver = Defines.TIMEOVER;
			}

			thisStep = 0;
			thisHp = hp;
		}

		public bool IsBoss(int level)
		{
			//return true;
			//return false;
			return (level != 0) && ((level % (Defines.MONSTER_STEP + 1)) == 0);
		}
	}

	private PlayStage	m_PlayStage = new PlayStage();	//진행 정보


	public void Load()
	{
		m_PlayStage.leave = (BossUtil.Player.GetInt(key_leave, 0) != 0) ? true : false;
		m_PlayStage.level = -1;
		dropCoinEffect = Resources.Load(Defines.PARTICLE_PATH+"dropCoin") as GameObject;
	}

	static public void Save(int level, int step)
	{
		BossUtil.Player.SetInt(key_level, level);
		BossUtil.Player.SetInt(key_step, step);
	}

	private PlayStage GetStageInfo(int level)
	{
		return new PlayStage(m_Girl, GlobalDataManager.Instance.GetStage(level));
	}

	public void SetUp(Hero hero, Back back, Monster monster, Girl girl, UI ui, UITexture girls, UIPanel boardPanel)
	{
		m_Hero = hero;
		m_Back = back;
		m_Monster = monster;
		m_Girl = girl;
		//this.girls = girls;
        m_UI = ui;
		this.boardPanel = boardPanel;
	}

	//
	// 레벨이나 스텝이 변경되면 호출.
	//
	public void LoadPlayStage(int level = -1, int step = -1)
	{
		//기록
		if (level == -1) level = BossUtil.Player.GetInt(key_level, 0);
		if (step == -1) step = 0;

		//이미 불려진 단계
		if ((m_PlayStage.leave != true) && (m_PlayStage.level == level) && (m_PlayStage.thisStep == step)) return;

		if (m_PlayStage.leave == true)
		{
			level--;
			step = 0;
		}

		//레벨 정보 가져오기
		PlayStage playStage = GetStageInfo(level);
		if (playStage == null) {
			BossDebug.LogError("level:"+level+"에 해당해는 내용을 찾을 수 없어요");
			return;
		}

		//내용 입력
		bool leave = m_PlayStage.leave;
		m_PlayStage = playStage;
		m_PlayStage.leave = leave;
		m_PlayStage.thisStep = step;
		m_UI.SetState(m_PlayStage.isBoss ? UI.Type._Boss : UI.Type._Monster);
		m_UI.Get().SetUp(m_PlayStage.hp, level, m_PlayStage.timeOver, m_PlayStage.leave);
		BossUtil.Player.SetInt(key_level, m_PlayStage.level);

		//그림
		//NGUITools.SetActive(girls.gameObject, m_PlayStage.IsBoss(level));
		m_Back.Set(level, m_PlayStage.isBoss);
		m_Monster.Set(level, step, m_PlayStage.isBoss);
		m_Girl.Set(level, step, m_PlayStage.isBoss);
		if (m_PlayStage.leave == true)
		{
			m_PlayStage.level++;
		}

		//if (m_PlayStage.IsBoss(level))
		//{
		//	SetTexture(girls, m_PlayStage.resourcePath);
		//	for (int i=0 ; i<monster.Count ; i++)
		//	{
		//		NGUITools.DestroyImmediate(monster[i].gameObject);
		//	}
		//	monster.Clear();
		//	GameObject monsterPrefabs = Resources.Load(Defines.MONSTER_PATH) as GameObject;
		//	for (int i=0 ; i<m_PlayStage.step - 1 ; i++)
		//	{
		//		GameObject obj = NGUITools.AddChild(girls.gameObject, monsterPrefabs);
		//		obj.name = i.ToString();
		//		monster.Add(obj.GetComponent<UITexture>());
		//		SetTexture(monster[i], m_PlayStage.resourcePath+"_"+i.ToString());
		//		monster[i].depth = i + 1;
		//	}

		//	//SetTexture(monster, m_PlayStage.resourcePath+Defines.MONSTER_PATH);
		//	Resources.UnloadUnusedAssets();
		//}
		SetStep(m_PlayStage.thisStep);
	}

	////
	//// 텍스쳐 셋힝
	////
	//private void SetTexture(UITexture Texture, string path)
	//{
	//	if ((Texture.mainTexture == null) ||
	//		(Texture.mainTexture.Equals(path) == false))
	//	{
	//		Texture.mainTexture = Resources.Load(Defines.BOARD_PATH+path) as Texture;
	//	}
	//}

	//
	// 스텝 셋힝
	//
	private void SetStep(int step)
	{
		if (step != Mathf.Clamp(step, 0, m_PlayStage.step))
		{
			BossDebug.LogError("사용 할 수 없는 단계에요 : "+step);
			NextLevel();
			return;
		}

		if (step >= m_PlayStage.step)
		{
			if (m_PlayStage.isBoss) SoundManager.Instance.PlayBGM("share_monster_bgm");
			NextLevel();
			return;
		}

		m_PlayStage.thisStep = step;

		SetHp(m_PlayStage.hp);
		BossUtil.Player.SetInt(key_step, m_PlayStage.thisStep);
	}

	private void NextLevel()
	{
		//CoroutineManager.Instance.StopCoroutine("Count");
		LoadPlayStage(m_PlayStage.level + (m_PlayStage.leave ? 0 : 1), 0);
		count = false;
	}

	//
	// hp 셋힝
	//
	private void SetHp(decimal hp)
	{
		//if (!hpBar || !hpPoint)
		//{
		//	BossDebug.LogError("LoadPlayStage 이후 사용 할 수 있어요");
		//	return;
		//}

		if (hp <= 0) hp = 0;
		m_PlayStage.thisHp = hp;
		m_UI.Get().SetHP(hp);
		//BossUtil.Animation.Pung(this.hpPoint.gameObject);
		if (hp == 0)
		{
			Kill();
			
		}
	}

	//private void MakeHole(float 

	private GameObject	dropCoinEffect;

	private void Kill()
	{
		Inventory.Instance.AddCoin(m_PlayStage.coin);
		//m_Stage.SetCoin(Inventory.Instance.Coin);
		BossUtil.Animation.Particle(new Vector3(0.0f, -300.0f), dropCoinEffect);

		SoundManager.Instance.PlayEffect(m_PlayStage.isBoss ? "share_boss_die" : "share_monster_die");

		if (m_PlayStage.isBoss) {
			m_Girl.Undress();
			NextStep();
		}
		else { 
			m_Hero.LockOnly(true);
			m_Monster.Die();
		}
	}

	public void NextStep()
	{
		SetStep(m_PlayStage.thisStep + 1);

		NetworkManager.Instance.Send("set", iTween.Hash(
			                         "do", "set",
			                         "level", m_PlayStage.level,
			                         "step", m_PlayStage.thisStep,
			                         "coin", m_Hero.GetCoin()));
	}

	public void Attack(decimal damage, bool auto = false)
	{
		SetHp(m_PlayStage.thisHp - damage);



		//if (!auto) {
		//	iTween tween = boardPanel.gameObject.GetComponent<iTween>();
		//	if (tween != null) {
		//		UnityEngine.Object.Destroy(tween);
		//		boardPanel.gameObject.transform.localPosition = Vector3.zero;
		//	}

		//	float min = Defines.BOARD_SHAKE_FORCE / 2;
		//	float max = Defines.BOARD_SHAKE_FORCE;

		//	iTween.ShakePosition(
		//		boardPanel.gameObject,
		//		new Vector3(
		//			Random.Range(min, max), Random.Range(min, max), Random.Range(min, max)
		//		),
		//		Defines.BOARD_SHAKE_TIME);
		//}

		if (m_PlayStage.isBoss) {
			float lostHpp = (float)((m_PlayStage.hp - m_PlayStage.thisHp) / m_PlayStage.hp);
			Mathf.Clamp(lostHpp, 0.0f, 1.0f);
			m_Girl.SetHole(lostHpp);
			if (!auto) SoundManager.Instance.PlayEffect("share_boss_hit");
		}
		else {
			if (!auto) {
				SoundManager.Instance.PlayEffect("share_monster_hit");
				m_Monster.Hit();
			}
		}

		//m_Girl.SetHole(1.0f - fill);
		//m_Monster.Hit();
	}

	bool		timeAlert = false;
	bool		count = false;
	float		start;
	float		alert;

	public void Management()
	{
		if (count == false) return;

		float per = (Time.time - start) / m_PlayStage.timeOver.Value;
		per = Mathf.Clamp(per, 0.0f, 1.0f);
		m_UI.GetBoss().SetTimer(per);

		if (timeAlert == false && Time.time > alert) {
			timeAlert = true;
			SoundManager.Instance.PlayEffect("boss_timeAlert");
		}

		//끝
		if (per == 1.0f)
		{
			count = false;
			TimeOver();
		}

				//		float per = (Time.time - start) / time;
				//per = Mathf.Clamp(per, 0.0f, 1.0f);
				//timer.fillAmount = per;
				//if (per == 1.0f) break;
	}

	public void Count() {
		start = Time.time;
		alert = start + m_PlayStage.timeOver.Value - Defines.TIMEALERT;
		count = true;
		timeAlert = false;

		//CoroutineManager.Instance.CancelInvoke("TimeOver");
		//CoroutineManager.Instance.Invoke("TimeOver", m_PlayStage.timeOver.Value);

		//CoroutineManager.Instance.StopCoroutine("Count_");
		//CoroutineManager.Instance.StartCoroutine(Count_());
	}

	public void TimeOver()
	{
		m_Girl.Reset();

		//체력
		m_PlayStage.thisStep = 0;
		SetHp(m_PlayStage.hp);

		//시간
		Count();
	}

	public void Sorry()
	{
		count = false;
		m_PlayStage.leave = true;
		BossUtil.Player.SetInt(key_leave, 1);
		LoadPlayStage(m_PlayStage.level);
		SoundManager.Instance.PlayBGM("share_monster_bgm");
	}

	public void Fight()
	{
		m_PlayStage.leave = false;
		BossUtil.Player.SetInt(key_leave, 0);
		m_PlayStage.level--;
		LoadPlayStage(m_PlayStage.level + 1);
		SoundManager.Instance.PlayBGM("share_boss_bgm");
	}


}