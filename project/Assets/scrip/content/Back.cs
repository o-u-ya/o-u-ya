﻿using UnityEngine;
using System.Collections;

public class Back : MonoBehaviour {

	private BackData		backData;

	[SerializeField] private UISprite		sprite;
	[SerializeField] private UITexture		texture;

	private int		backId = -1;

	public void Load() {
		backData = BossUtil.XmlToClass<BackData>("BackData");
	}

	public void Set(int level, bool isBoss) {
		int temp = ((level - 1) / Defines.BACK_LEVEL) % backData.Items.Length;
		if (backId == temp) return;
		SoundManager.Instance.PlayBGM("share_monster_bgm");
		backId = temp;
		bool isTexture = string.Compare(backData.Items[ backId ].type, "Texture") == 0 ? true : false;
		NGUITools.SetActive(sprite.gameObject, !isTexture);
		NGUITools.SetActive(texture.gameObject, isTexture);


		if (isTexture) {
			texture.mainTexture = Resources.Load(Defines.BACK_PATH + backData.Items[ backId ].image) as Texture2D;
		}
		else {
			sprite.spriteName = backData.Items[ backId ].image;
		}
	}
}
