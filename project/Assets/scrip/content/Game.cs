﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour 
{
    public enum ItemTapType
    {
        eITT_Coin,
        eITT_Cash,
    }

	private Stage		m_Stage = new Stage();
	private Hero		m_Hero = new Hero();
    private Item        m_Item = new Item();
	private Monster		m_Monster = new Monster();
	private Girl		m_Girl = new Girl();
	[SerializeField] private Back		m_Back;
    [SerializeField] private UI         m_UI;
	[SerializeField] private UIPanel	boardPanel;
	[SerializeField] private UITexture	girls;
	[SerializeField] private UISprite	monster;
    #region Shop

    [SerializeField] private GameObject shop;
    [SerializeField] private GameObject shopbtn;
    [SerializeField] private GameObject shop_CoinItemTap;
    [SerializeField] private GameObject shop_CashItemTap;
    [SerializeField] private ShopData ShopItem;
    [SerializeField] private UIGrid m_Grid;

    private bool m_bShopOnOff = false;
    private ItemTapType m_eItemType = ItemTapType.eITT_Coin;

    // 현재 인벤토리 정보
    private List<ShopData> m_pShopDataList = new List<ShopData>();
    private List<GameObject> m_ltItemList = new List<GameObject>();

    #endregion // Shop

	void Start()
	{
        AddEvent();
		
        Load();
	}

	void Load()
	{
		//Load Data
		m_Stage.Load();
		m_Hero.Load();
		m_Back.Load();
        m_Item.Load();
		m_Girl.Load();

		//Set Game
		m_UI.SetUp(m_Stage);
		m_Monster.SetUp(m_Stage, m_Hero, monster);
		m_Stage.SetUp(m_Hero, m_Back, m_Monster, m_Girl, m_UI, girls, boardPanel);
		m_Hero.SetUp(m_Stage, m_Item, m_UI);
		m_Girl.SetUp(m_Hero, girls, boardPanel);
		m_Stage.LoadPlayStage();

        AdMobManager.Instance.ShowBannerView();
	}

	void Update()
	{
		m_Hero.Management();
		m_Monster.Management();
		m_Stage.Management();
		Exit();
	}

    void AddEvent()
    {
        BossUtil.Button.AddEvent(shopbtn, OnClick_Shop);
        BossUtil.Button.AddEvent(shop_CoinItemTap, OnClick_Tap);
        BossUtil.Button.AddEvent(shop_CashItemTap, OnClick_Tap);
        Inventory.Instance.GetInventoryEventHandler += CallBack_GetInventory;
        Inventory.Instance.UpdateInventoryItemEventHandler += CallBack_UpdateItem;
    }

    void ClearItemList()
    {
        for (int nCnt = 0; nCnt < m_ltItemList.Count; nCnt++)
        {
            DestroyImmediate(m_ltItemList[nCnt]);
        }
        m_ltItemList.Clear();
        m_pShopDataList.Clear();

    }

    void CreateItemList()
    {
        // 아이템 테이블에 있는 값과 비교한다.
        GameObject pObject = null;
        int nGruop = 1;
        int nCount = 0;
        m_pShopDataList.Clear();
        if (Inventory.Instance.InventoryDataList == null || Inventory.Instance.InventoryDataList.Count == 0)
        {
            // 인벤토리 정보를 받지 못하였다면 기본정보로 뿌림.
            for (int nCnt = 0; nCnt < m_Item.ConvertItemList.Count; nCnt++)
            {
                int nitemid = m_Item.ConvertItemList[nCnt].id;
                int nitemgroup = m_Item.ConvertItemList[nCnt].itemgroup;
                int nitemlevel = m_Item.ConvertItemList[nCnt].itemlevel;

                if (nitemgroup == nGruop && nitemlevel == 1)
                {
                    pObject = Instantiate(ShopItem.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
                    pObject.transform.SetParent(m_Grid.transform);
                    pObject.transform.localScale = Vector3.one;
                    pObject.transform.localPosition = Vector3.one;
                    pObject.GetComponent<ShopData>().SetData(
						m_Item.GetItem(nitemid), 
						m_Item.MinID_MaxID(false, nitemgroup)
					);
                    pObject.SetActive(true);
                    m_pShopDataList.Add(pObject.GetComponent<ShopData>());
                    m_ltItemList.Add(pObject);
                    nGruop++;
                }
            }
        }
        else
        {
            // 인벤토리 정보를 받았다면
            for (int nCnt = 0; nCnt < m_Item.ConvertItemList.Count; nCnt++)
            {
                int nitemid = m_Item.ConvertItemList[nCnt].id;
                int nitemgroup = m_Item.ConvertItemList[nCnt].itemgroup;
                int nitemlevel = m_Item.ConvertItemList[nCnt].itemlevel;

                if (nitemgroup == nGruop && nitemlevel == 1)
                {
                    pObject = Instantiate(ShopItem.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
                    pObject.transform.SetParent(m_Grid.transform);
                    pObject.transform.localScale = Vector3.one;
                    pObject.transform.localPosition = Vector3.one;
                    pObject.GetComponent<ShopData>().SetData(m_Item.GetItem(nitemid), m_Item.MinID_MaxID(false, nitemgroup));
                    pObject.SetActive(true);
                    m_pShopDataList.Add(pObject.GetComponent<ShopData>());
                    m_ltItemList.Add(pObject);
                    nGruop++;
                }
            }

            for (int nCnt = 0; nCnt < m_pShopDataList.Count; nCnt++)
            {
                if(Inventory.Instance.InventoryDataList.Count > nCount)
                {
                    int nitemid = int.Parse(Inventory.Instance.InventoryDataList[nCount].itemid);
                    int nitemgroup = int.Parse(Inventory.Instance.InventoryDataList[nCount].itemgroup);

                    if (nitemgroup == (nCnt + 1))
                    {
                        m_pShopDataList[nCnt].SetData(m_Item.GetItem(nitemid), m_Item.MinID_MaxID(false, nitemgroup));
                        nCount++;
                    }
                }
                
            }
        }
        
        m_Grid.Reposition();
        m_Grid.transform.localPosition = new Vector2(0f, 200f); // 자꾸 지맘대로 바뀐다

        #region OLD

        //int nGroup = 0;
        //for (int nCnt = 0; nCnt < m_Item.GetItemList().Count; nCnt++)
        //{
        //    // 테스트 용도로 그룹별 첫번째 아이템을 상점에 등록했다. 
        //    // 유저가 구입한 아이템의 id값을 증가시켜 서버에 등록하는 방식으로 해야할 것 같다.
        //    // 서버 인벤토리에 들어가야 하는 값은 결국 아이템 그룹 하나당 한개로 되어진다. 
        //    // 15.04.09 - RCH

        //    if(m_Item.GetItemList()[nCnt].itemgroup > nGroup)
        //    {
        //        pObject = Instantiate(ShopItem.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
        //        pObject.transform.SetParent(m_Grid.transform);
        //        pObject.transform.localScale = Vector3.one;
        //        pObject.GetComponent<ShopData>().SetData(m_Item.GetItem(nCnt));
        //        pObject.SetActive(true);
        //        nGroup++;
        //    }
        //    //pObject = Instantiate(ShopItem.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
        //    //pObject.transform.SetParent(m_Grid.transform);
        //    //pObject.transform.localScale = Vector3.one;
        //    //pObject.GetComponent<ShopData>().SetData(m_Item.GetItem(nCnt));
        //    //pObject.SetActive(true);
        //}
        #endregion // OLD

    }

    void SetData()
    {
        ClearItemList();

        switch(m_eItemType)
        {
            case ItemTapType.eITT_Coin:
                {
                    CreateItemList();
                }
                break;
            case ItemTapType.eITT_Cash:
                {
                    
                }
                break;
        }
    }

    #region OnClickEvent

    void OnClick_Shop(GameObject o)
    {
        m_bShopOnOff = !m_bShopOnOff;

        if (m_bShopOnOff)
        {
            iTween.MoveTo(shop, iTween.Hash("x", -630f, "time", 0.5f, "islocal", true, "easeType", "linear"));
        }
        else
        {
            iTween.MoveTo(shop, iTween.Hash("x", -1289f, "time", 0.5f, "islocal", true, "easeType", "linear"));
        }
    }

    void OnClick_Tap(GameObject o)
    {
        if(o == shop_CoinItemTap)
        {
            m_eItemType = ItemTapType.eITT_Coin;
        }
        else if( o == shop_CashItemTap)
        {
            m_eItemType = ItemTapType.eITT_Cash;
        }

        SetData();
    }

    #endregion // OnClickEvent

    #region CallBack

    void CallBack_UpdateItem(int itemgroup, int itemid)
    {
        for (int nCnt = 0; nCnt < m_pShopDataList.Count; nCnt++)
        {
            if (itemgroup == (nCnt + 1))
            {
                m_pShopDataList[nCnt].SetData(m_Item.GetItem(itemid), m_Item.MinID_MaxID(false, itemgroup));
            }
        }
    }

    void CallBack_GetInventory()
    {
        #region TEST
        //for (int nCnt = 0; nCnt < Inventory.Instance.InventoryDataList.Count; nCnt++)
        //{
        //    BossDebug.LogError("InventoryDataList.itemGroup : " + Inventory.Instance.InventoryDataList[nCnt].itemgroup);
        //    BossDebug.LogError("InventoryDataList.itemid : " + Inventory.Instance.InventoryDataList[nCnt].itemid);
        //    BossDebug.LogError("InventoryDataList.itemlevel : " + Inventory.Instance.InventoryDataList[nCnt].itemlevel);
        //    BossDebug.LogError("InventoryDataList.usedtype : " + Inventory.Instance.InventoryDataList[nCnt].usedtype);
        //}

        //테스트코드
        //Inventory.Instance.InventoryDataList.Clear();
        #endregion //TEST

        SetData();

        //CreateItemList();
    }

    #endregion // CallBack

    void Exit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Message.Instance.Show("그만 할래요?", delegate { Application.Quit(); });
        }
    }

    #region OLD

    //public void TESTFunc()
    //{
    //    BossDebug.Log("Call TESTFunc");

    //    for (int nCnt = 0; nCnt < Inventory.Instance.InventoryDataList.Count; nCnt++)
    //    {
    //        BossDebug.Log("itemGroup : " + Inventory.Instance.InventoryDataList[nCnt].itemGroup);
    //        BossDebug.Log("itemid : " + Inventory.Instance.InventoryDataList[nCnt].itemid);
    //        BossDebug.Log("itemlevel : " + Inventory.Instance.InventoryDataList[nCnt].itemlevel);
    //        BossDebug.Log("usedtype : " + Inventory.Instance.InventoryDataList[nCnt].usedtype);
    //    }
    //}
	
	//private int m_stage = 0;

	//void Update()
	//{
	//	if (Input.GetKeyDown(KeyCode.LeftArrow))
	//	{
	//		m_stage--;
	//		m_Stage.LoadPlayStage(girls, monster, hpBar, hpPoint, m_Hero.Level, m_stage);
	//	}

	//	if (Input.GetKeyDown(KeyCode.RightArrow))
	//	{
	//		m_stage++;
	//		m_Stage.LoadPlayStage(girls, monster, hpBar, hpPoint, m_Hero.Level, m_stage);
	//	}

	//	if (Input.GetKeyDown(KeyCode.Escape))
	//	{
	//		Message.Instance.Show("하하하");
	//	}
    //}

    #endregion //OLD

}
