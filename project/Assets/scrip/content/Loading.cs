﻿using UnityEngine;
using System;
using System.Collections;
using LitJson;

public class Loading : MonoBehaviour 
{
	void Start()
	{
		SoundManager.Instance.Load();

		string uuid = SystemInfo.deviceUniqueIdentifier.Substring(0, 32);
		NetworkManager.Instance.Send(
			"login",
			iTween.Hash("do", "login", "uuid", uuid),
			delegate (string result, SimpleJSON.JSONNode data) {
				Inventory.Save(decimal.Parse(data["coin"]));
				Stage.Save(int.Parse(data["level"]), int.Parse(data["step"]));
				NetworkManager.Instance.SetId(int.Parse(data["id"]));

                Inventory.Instance.GetInventory(int.Parse(data["id"]));
				PageManager.Instance.Open(ePageType._Game);
			},
			delegate (string err) {
				PageManager.Instance.Open(ePageType._Game);
			}
		);
	} 
}
