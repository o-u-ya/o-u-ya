﻿using UnityEngine;
using System.Collections;

public class TabDamage : MonoBehaviour {

	[SerializeField] private UILabel text;

	public void Set(decimal point) {
		text.text = BossUtil.DecimalToPromise(point);
		Destroy(gameObject, 0.5f);
	}
}
