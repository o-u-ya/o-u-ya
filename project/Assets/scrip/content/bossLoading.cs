﻿using UnityEngine;
using System.Collections;

public class bossLoading : MonoBehaviour 
{
    private Hero            m_Hero;

	[SerializeField] private GameObject progress;
	[SerializeField] private GameObject start;

    public void SetUp(Hero hero)
    {
        m_Hero = hero;
    }

	public void Start()
	{
		progress.SetActive(true);
		start.SetActive(false);
		UIEventListener.Get(start).onClick += Close; 
		SoundManager.Instance.PlayBGM("share_boss_bgm");
	}

	public void Complete()
	{
		progress.SetActive(false);
		start.SetActive(true);
	}

	private void Close(GameObject obj)
	{
		PageManager.Instance.Close(ePageType._BossLoading);
        m_Hero.Lock(false);
	}
}
