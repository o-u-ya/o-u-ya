﻿using UnityEngine;
using System.Collections;

public class ShopData : MonoBehaviour {


    [SerializeField]
    private UISprite m_sp_icon;
    [SerializeField]
    private UILabel m_lb_Name;
    [SerializeField]
    private UILabel m_lb_Info;
    [SerializeField]
    private UILabel m_lb_Price;
    [SerializeField]
    private GameObject m_btn_Buy;

    private Item.ConvertItem m_pItem_ConvertItem;
    private int m_nGroupMaxID;

    void Start()
    {
        ButtonEvent();
    }

    void ButtonEvent()
    {
        BossUtil.Button.AddEvent(m_btn_Buy, OnClick_Buy);
    }
    public void SetData(Item.ConvertItem pData, int GroupMaxID)
    {
        if (pData == null)
        {
            BossDebug.LogError("아이템 테이블에 문제가 있다");
            return;
        }

        m_pItem_ConvertItem = pData;
        m_nGroupMaxID = GroupMaxID;

        if(m_pItem_ConvertItem == null)
        {
            BossDebug.LogError("데이터에 넘어오는것에 문제가 있다.");
            return;
        }

        m_sp_icon.spriteName = m_pItem_ConvertItem.image;
        m_lb_Name.text = m_pItem_ConvertItem.name;
        m_lb_Info.text = m_pItem_ConvertItem.iteminfo;
        m_lb_Price.text = m_pItem_ConvertItem.price.ToString();
    }

    public void OnClick_Buy(GameObject o)
    {
        //BossDebug.Log("Inventory.Instance.Coin : " + Inventory.Instance.Coin);
        //BossDebug.Log("m_pItem_ConvertItem.price : " + m_pItem_ConvertItem.price);
        if(m_nGroupMaxID < m_pItem_ConvertItem.id)
        {
            Message.Instance.Show("최고레벨입니다.");
            return;
        }

        if (Inventory.Instance.Coin >= m_pItem_ConvertItem.price)
        {
            // 구매가능
            //if(Online)
            //{
            // 구매 보내기
            NetworkManager.Instance.Send("inventory", iTween.Hash(
                             "do", "inventory_update",
                             "itemgroup", m_pItem_ConvertItem.itemgroup,
                             "itemid", m_pItem_ConvertItem.id,
                             "itemlevel", m_pItem_ConvertItem.itemlevel,
							 "price", m_pItem_ConvertItem.price,
                             "usedtype", 1),
                             delegate(string result, SimpleJSON.JSONNode data)
                             {
                                 BossDebug.Log(data.ToString());

                                 Inventory.Instance.AddCoin(-m_pItem_ConvertItem.price);
                                 Inventory.Instance.UpdateInventoryItem(int.Parse(data["itemgroup"]), int.Parse(data["itemid"]));
                             });


            //}
            // 코인 차감
            
        }
        else
        {
            Message.Instance.Show("돈이 부족합니다!\n충전하시겠습니까?");
        }
        
    }

    

}
