﻿using UnityEngine;
using System.Collections;

public class ExitEffect : NextPageEffect 
{
	private float	timeOut;
	private float	timeIn;
	private float	timeWait;

	public ExitEffect(string path, float timeOut, float timeIn, float timeWait) : base(path)
	{
		this.timeIn = timeIn;
		this.timeOut = timeOut;
		this.timeWait = timeWait;
	}

	public override void Show(ePageType ePageType)
	{
		if (!base.CheckAbleShow()) return;
		PageManager.Instance.StartCoroutine(Do(ePageType));
	}

	protected override IEnumerator Do(ePageType ePageType)
	{
		GameObject Obj = base.Add();

		//현재 표시되고있는 페이지가 있다면 Out
		if (PageManager.Instance.IsHaveReadPage())
		{
			TweenAlpha.Begin(Obj, timeOut, 1.0f, 0.0f);
			yield return new WaitForSeconds(timeOut);
			PageManager.Instance.CloseAll();
		}

		//표시하고있는 페이지가 없다면 바로 In
		PageManager.Instance.Open(ePageType);
		TweenAlpha.Begin(Obj, timeIn, 0.0f, 1.0f);
		yield return new WaitForSeconds(timeIn);

		//대기
		yield return new WaitForSeconds(timeWait);

		//까맣게
		TweenAlpha.Begin(Obj, timeOut, 1.0f, 0.0f);
		yield return new WaitForSeconds(timeOut);

		//종료
		Application.Quit();
		yield break;
	}
}
