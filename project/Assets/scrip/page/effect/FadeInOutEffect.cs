﻿using UnityEngine;
using System.Collections;

public class FadeInOutEffect : NextPageEffect 
{
	private float	timeIn;
	private float	timeOut;

	public FadeInOutEffect(string path, float timeOut, float timeIn) : base(path)
	{
		this.timeOut = timeOut;
		this.timeIn = timeIn;
	}

	public override void Show(ePageType ePageType)
	{
		if (!base.CheckAbleShow()) return;
		PageManager.Instance.StartCoroutine(Do(ePageType));
	}

	protected override IEnumerator Do(ePageType ePageType)
	{
		GameObject Obj = base.Add();

		//현재 표시되고있는 페이지가 있다면 Out
		if (PageManager.Instance.IsHaveReadPage())
		{
			TweenAlpha.Begin(Obj, timeOut, 1.0f, 0.0f);
			yield return new WaitForSeconds(timeOut);
			if (PageManager.Instance.IsOnlyonePage(ePageType))
			{
				PageManager.Instance.CloseAll();
			}
		}

		//표시하고있는 페이지가 없다면 바로 In
		PageManager.Instance.Open(ePageType);
		TweenAlpha.Begin(Obj, timeIn, 0.0f, 1.0f);
		yield return new WaitForSeconds(timeIn);
		NGUITools.Destroy(Obj);
		yield break;
	}
}
