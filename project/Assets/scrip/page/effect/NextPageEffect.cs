﻿using UnityEngine;
using System.Collections;

public class NextPageEffect 
{
	public string		path;
	public float		time;

	protected GameObject Source;

	public NextPageEffect(string path)
	{
		this.path = path;
		Load();
	}

	public NextPageEffect(float time)
	{
		this.time = time;
	}

	public void Load()
	{
		if (string.IsNullOrEmpty(path))
		{
			BossDebug.Log("path가 비어있어요. 리소스 등록에 실패했어요.");
			return;
		}

		if (Source != null)
		{
			BossDebug.LogWarning("이미 리소스가 불려져있어요. 확인이 필요해요.");
			return;
		}

		Source = Resources.Load(path) as GameObject;
	}

	public GameObject Add()
	{
		GameObject Obj = NGUITools.AddChild(PageManager.Instance.Parent, Source);
		return Obj;
	}

	protected bool CheckAbleShow(bool checkSource = true)
	{
		if (!PageManager.Instance.IsSetParent())
		{
			return false;
		}

		if (checkSource && (Source == null))
		{
			BossDebug.LogError("리소스 로드 이후 Show가 가능해요.:)");
			return false;
		}

		return true;
	}

	public virtual void Show(ePageType ePageType)
	{
		if (!CheckAbleShow()) return;
		PageManager.Instance.StartCoroutine(Do(ePageType));
	}

	protected virtual IEnumerator Do(ePageType ePageType)
	{
		/*
		 * 여기에 효과를 구현
		 */
		yield break;
	}
}
