﻿using UnityEngine;
using System.Collections;

public class PungEffect : NextPageEffect
{
	public PungEffect(float time) : base(time)
	{
		this.time = time;
	}

	public override void Show(ePageType ePageType)
	{
		if (!base.CheckAbleShow(false)) return;
		PageManager.Instance.StartCoroutine(Do(ePageType));
	}

	protected override IEnumerator Do(ePageType ePageType)
	{
		GameObject obj = PageManager.Instance.Open(ePageType);
		obj.transform.localScale = Vector3.zero;
		NGUITools.DestroyImmediate(obj.GetComponent(typeof(iTween)));
		iTween.ScaleTo
		(	
			obj,
			iTween.Hash
			(
				"scale", Vector3.one,
				"time", time,
				"easetype", iTween.EaseType.easeOutBack
			)
		);
		yield return new WaitForSeconds(time);
		yield break;
	}
}

public class PungReverseEffect : NextPageEffect
{
	public PungReverseEffect(float time) : base(time)
	{
		this.time = time;
	}

	public override void Show(ePageType ePageType)
	{
		if (!base.CheckAbleShow(false)) return;
		PageManager.Instance.StartCoroutine(Do(ePageType));
	}

	protected override IEnumerator Do(ePageType ePageType)
	{
		GameObject obj = PageManager.Instance.Get(ePageType);
		if (obj == null)
		{
			BossDebug.LogWarning("이미 닫힌 상태에요");
			yield break;
		}

		//obj.transform.localScale = Vector3.one;
		NGUITools.DestroyImmediate(obj.GetComponent(typeof(iTween)));
		iTween.ScaleTo
		(	
			obj,
			iTween.Hash
			(
				"scale", Vector3.zero,
				"time", time,
				"easetype", iTween.EaseType.easeInBack
			)
		);
		yield return new WaitForSeconds(time);
		PageManager.Instance.Close(ePageType);
		yield break;
	}
}
