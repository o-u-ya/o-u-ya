﻿using UnityEngine;
using System.Collections;

public class NextPageFromTime : MonoBehaviour 
{
	[SerializeField]
	private float		wait = 2.0f;
	[SerializeField]
	private ePageType	eNextPage;
	[SerializeField]
	private eNextPageEffect	eEffect = eNextPageEffect._Fade_InOut;


	void Start()
	{
		StartCoroutine(Do());
	}

	IEnumerator Do()
	{
		yield return new WaitForSeconds(wait);
		PageManager.Instance.Open(eNextPage, eEffect);
		yield break;
	}
}
