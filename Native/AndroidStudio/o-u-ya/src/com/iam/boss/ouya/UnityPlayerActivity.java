package com.iam.boss.ouya;

import com.unity3d.player.*;

/**
 * @deprecated Use UnityPlayerNativeActivity instead.
 */
public class UnityPlayerActivity extends UnityPlayerNativeActivity { }