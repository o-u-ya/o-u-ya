ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
D:\Unity Project\ouya\Native\AndroidStudio\o-u-ya_by_AndroidStudio
        -                                                         

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .idea\
* .idea\.name
* .idea\compiler.xml
* .idea\copyright\
* .idea\copyright\profiles_settings.xml
* .idea\dictionaries\
* .idea\dictionaries\Ryu.xml
* .idea\misc.xml
* .idea\modules.xml
* .idea\o-u-ya.iml
* .idea\vcs.xml
* .idea\workspace.xml
* o-u-ya.iml
* ouya\
* ouya\.gitignore
* ouya\build.gradle
* ouya\src\
* ouya\src\main\
* ouya\src\main\java\
* ouya\src\main\java\com\
* ouya\src\main\java\com\iam\
* ouya\src\main\java\com\iam\boss\
* ouya\src\main\java\com\iam\boss\ouya\
* ouya\src\main\java\com\iam\boss\ouya\ouyaActivity.java
* settings.gradle

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets\
* libs\armeabi-v7a\libmain.so => app\src\main\jniLibs\armeabi-v7a\libmain.so
* libs\armeabi-v7a\libmono.so => app\src\main\jniLibs\armeabi-v7a\libmono.so
* libs\armeabi-v7a\libunity.so => app\src\main\jniLibs\armeabi-v7a\libunity.so
* libs\google-play-services.jar => app\libs\google-play-services.jar
* libs\unity-classes.jar => app\libs\unity-classes.jar
* libs\unity-plugin-library.jar => app\libs\unity-plugin-library.jar
* libs\x86\libmain.so => app\src\main\jniLibs\x86\libmain.so
* libs\x86\libmono.so => app\src\main\jniLibs\x86\libmono.so
* libs\x86\libunity.so => app\src\main\jniLibs\x86\libunity.so
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
