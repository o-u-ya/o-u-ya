package com.iam.boss.ouya;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.util.Map;

/**
 * Created by 창호 on 2015-05-25.
 */
public class ouyaActivity extends UnityPlayerNativeActivity
{
    static final String TAG = "ouyaActivity";

    public static Context m_SelfContext;
    public static ouyaActivity ms_MainActivity = null;

    public static int m_CurrentApiVersion = 19;
    public static String ms_Version = "0";
    public static Context m_MainContext;



    protected void onCreate(Bundle savedInstanceState)
    {
        ms_MainActivity = this;
        m_SelfContext = this;
        m_CurrentApiVersion = android.os.Build.VERSION.SDK_INT;

        

        // ----------------------------------------------------------------
        // 유니티 생성
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        Log.d(TAG, "UnityPlayer Create After 1");
        m_MainContext = this;

        try
        {
            PackageInfo i = this.getPackageManager().getPackageInfo(
                    this.getPackageName(), 0);
            ms_Version = i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "packetname exception : " + e);
        }

        Log.d(TAG, "UnityPlayer Create After 2");
        if (null == savedInstanceState) {
            WindowManager.LayoutParams params = getWindow().getAttributes();
            // 밝기 정보
            // params.screenBrightness = 1.0f;// brightness 정보를 변경 해주고
            getWindow().setAttributes(params);// 속성 set을 해준다
            getWindow()
                    .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        Log.d(TAG, "UnityPlayer Create After 3");
        String classNames[] = { "com.iam.boss.ouya.ouyaActivity", };
        copyPlayerPrefs(this, classNames);

        Log.d(TAG, "OnCreate() OK ");

    }

    protected void onPause()
    {
        super.onPause();
    }

    protected void onResume()
    {
        super.onResume();
    }

    protected void onDestroy()
    {
        super.onDestroy();
//        if (niapHelper != null) {
//            Log.d("DEBUG", "release helper");
//            niapHelper.terminate();
//            niapHelper = null;
//        }
//        // Log.d(TAG, "Destroying helper.");
//        if (mIabHelper != null)
//            mIabHelper.dispose();
//        mIabHelper = null;

    }

    static protected void copyPlayerPrefs(Context context,
                                          String[] activityClassNames)
    {
        // UnityPlayer uses PackageName (bundle identifier) as PlayerPrefs
        // identifier, starting from Unity 3.4.
        SharedPreferences packagePrefs = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);

        // If PlayerPrefs<package_name> already exists there is no need to
        // copy the old values; they might in fact be stale data.
        if (!packagePrefs.getAll().isEmpty())
            return;

        // Loop through the Activities and copy the contents (if any) of
        // associated PlayerPrefs (Unity 3.3 and earlier).
        SharedPreferences.Editor playerPrefs = packagePrefs.edit();
        for (String name : activityClassNames) {
            SharedPreferences prefs = context.getSharedPreferences(name,
                    Context.MODE_PRIVATE);
            java.util.Map<String, ?> keys = prefs.getAll();
            if (keys.isEmpty())
                continue;
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                Object value = entry.getValue();
                if (value.getClass() == Integer.class)
                    playerPrefs.putInt(entry.getKey(), (Integer) value);
                else if (value.getClass() == Float.class)
                    playerPrefs.putFloat(entry.getKey(), (Float) value);
                else if (value.getClass() == String.class)
                    playerPrefs.putString(entry.getKey(), (String) value);
            }
            playerPrefs.commit();
        }
    }
}
