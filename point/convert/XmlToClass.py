#-*- coding: euc-kr -*-
import sys
import os

#
# 파일 검색
#
def search(result, path, key = ''):
    if os.path.isdir(path) == False:
       return None

    find = []
    flist = os.listdir(path)
    for f in flist:
        next = os.path.join(path, f)
        if os.path.isdir(next):
            search(next)
        elif next.find(key) != -1:
           find.append(next)
    result.extend(find)

#
# cmd
#
def execute(cmd) :
    std_in, std_out, std_err = os.popen3(cmd)
    return std_out, std_err

# 프로그램 시작
XmlPath = 1
OutPath = 2
ArgvEnd = 3

# 실행인자 처리
if len(sys.argv) < ArgvEnd:
    print 'xml폴더 위치와 변환된 클래스들이 들어갈 폴더 위치를 지정 해야 해요'
    sys.exit()

#xml목록
XmlPathList = []
search(XmlPathList, sys.argv[XmlPath], '.xml')

#변환
for This in XmlPathList:
    #do
    temp = This[ len(sys.argv[XmlPath])+1 : len(This)-4 ]
    print temp+'.xml -> '+temp+'.cs'

    #xml -> xsd
    execute('xsd '+This+' /classes /out:'+sys.argv[XmlPath])

    #xsd -> class
    This = This[ :len(This)-3 ] + 'xsd'
    execute('xsd '+This+' /classes /out:'+sys.argv[OutPath])
    #xsd 제거
    os.remove(This)