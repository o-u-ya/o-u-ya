<?php
/**
 * Created by PhpStorm.
 * User: iamboss-home
 * Date: 2015-02-17
 * Time: 오후 8:42
 */

include 'code.php';
include 'dbc.php';

$get = array();
$data = array();
foreach($_POST as $key => $value) {$get[$key] = filter($value);}
$uuid = $get['uuid'];

if ($get['do'] == 'login')
{
    if ($uuid == null) exit($RESULT_EMPTY_PARAMS.'uuid');

    //찾기
    $result = FindUser($uuid);

    //계정생성
    if ($result->num_rows == 0)
    {
        $db->query("insert into user(uuid, level, step, coin) values('$uuid', 0, 0, '0')") or
            die ($RESULT_QUERY_ERROR.'계정 생성 실패,'.$db->error);
        $result = FindUser($uuid);
        if ($result == null || $result->num_rows == 0) die($RESULT_ERROR.'유저 생성은 성공했는데 찾을수가 없어요');
    }

    //로그인
    $items= $result->fetch_assoc();
    exit($RESULT_SUCCESS.json_encode($items));
}

else if ($get['do'] == 'reset')
{
    if ($uuid == null) exit($RESULT_EMPTY_PARAMS.'uuid');
    $result = FindUser($uuid);

    if ($result->num_rows == 0) die($RESULT_ERROR.'찾을수가 없어요');
    $user = $result->fetch_assoc();
    $result = $db->query("update user set level=0, step=0, coin=0 where id=$user[id] limit 1") or
        die ($RESULT_QUERY_ERROR.$db->error);
    $result = $db->query("delete from inventory where id=$user[id]") or
        die ($RESULT_QUERY_ERROR.$db->error);
    exit($RESULT_SUCCESS);
}

exit($RESULT_DO_NOTHING);

//
// 찾기
//
function FindUser($uuid)
{
    global $db;
    global $RESULT_QUERY_ERROR;

    $result = $db->query("select id, uuid, level, step, coin from user where uuid = '$uuid' limit 1") or
        die ($RESULT_QUERY_ERROR.$db->error);
    return $result;
}
