<?php
/**
 * Created by PhpStorm.
 * User: iamboss-home
 * Date: 2015-02-17
 * Time: 오후 8:19
 */
define ("DB_HOST", "localhost");       // set database host
define ("DB_USER", "root");            // set database user
define ("DB_PASS","qZqVUvZ2DEbdZr8F"); // set database password
define ("DB_NAME","o-u-ya");           // set database name

$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME) or die("Couldn't make connection.");
if ($db->connect_errno)
{
    printf("Connect failed: %s\n", $db->connect_error);
    exit();
}
$db->set_charset("utf8");

/*
 * 필터
 */
function filter($data) {

    global $db;

    $data = trim(htmlentities(strip_tags($data),ENT_COMPAT,"UTF-8"));

    if (get_magic_quotes_gpc())
        $data = stripslashes($data);

    $data = $db->real_escape_string($data);

    return $data;
}