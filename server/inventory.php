<?php
/**
 * Created by PhpStorm.
 * User: Ryu
 * Date: 2015-03-23
 * Time: 오후 10:46
 */

include 'code.php';
include 'dbc.php';

$get = array();
$data = array();
foreach($_POST as $key => $value) {$get[$key] = filter($value);}
$id = $get['id'];
$itemgroup = $get['itemgroup'];
$itemid = $get['itemid'];
$itemlevel = $get['itemlevel'];
$usedtype = $get['usedtype'];
$price = $get['price'];

// 아이템 종류(itemgroup) : 유일함
// 아이템 아이디(itemid) : 아이템 고유정보(종류에 상관없이) 유일한 정보지만 아직 사용을 안함... 어디에서 쓸까?
// 아이템 레벨(itemlevel) : 유일함
// 사용상태 (usedtype)
// 패시브 아이템일 경우 : true - 사용중, false - 사용중이지 않음.
// 액티브 아이템일 경우 : true - 구입함, false - 구입하지 않음.


if ($get['do'] == 'inventory_get')
{
    // 전체 아이템 목록을 서버로부터 받아서 로컬 저장.

    if ($id == null) exit($RESULT_EMPTY_PARAMS.'id');

    // 자기 아이템 찾기
    $result = FindInventory($id);

    if ($result->num_rows != 0)
    {
        $result = FindInventory($id);
        if ($result == null || $result->num_rows == 0) die($RESULT_ERROR.'인벤토리 생성은 성공했는데 찾을수가 없다');
    }

//    $items = $result->fetch_assoc();
    $myItemsArray = array();
    $nCnt = 0;
    while( $items= $result->fetch_assoc())
    {
        $myItemsArray[$nCnt] = $items;
        $nCnt++;
    }

    $jsonStr = json_encode($myItemsArray);
    exit($RESULT_SUCCESS.json_encode($myItemsArray));
}
else if ($get['do'] == 'inventory_update')
{
    if ($itemgroup == null) exit($RESULT_EMPTY_PARAMS.'itemgroup');
    if ($itemid == null) exit($RESULT_EMPTY_PARAMS.'itemid');
    if ($itemlevel == null) exit($RESULT_EMPTY_PARAMS.'itemlevel');
    if ($usedtype == null) exit($RESULT_EMPTY_PARAMS.'usedtype');
    if ($price == null) exit($RESULT_EMPTY_PARAMS.'price');
    if ($id == null) exit($RESULT_EMPTY_PARAMS.'id');
    //if ($price == null) exit($RESULT_EMPTY_PARAMS.'price');

//    $result = CheckCoin($userid);
//
//    if($result->num_rows == 0)
//    {
//        // db 에러
//    }
//    else
//    {
//        if($result->coin < $price)
//        {
//            // 돈이 부족
//        }
//    }

    // db에 등록된 아이템 찾기
    //$result = UpdateInventory($itemgroup, $id, $itemid, $itemlevel, $usedtype);
    $result = FindItem($id,$itemgroup);

    if ($result->num_rows == 0)
    {
        // 없다면 인벤토리에 아이템 정보 등록

        $db->query("insert into inventory(itemgroup, itemid, itemlevel, usedtype, id) values('$itemgroup', '$itemid'+1, '$itemlevel'+1, '$usedtype', '$id')") or
        die ($RESULT_QUERY_ERROR.'인벤토리 생성 실패,'.$db->error);
        $result = FindItem($id,$itemgroup);
        if ($result == null || $result->num_rows == 0) die($RESULT_ERROR.'인벤토리 생성은 성공했는데 찾을수가 없다');
    }
    else
    {
        // 아이템 업데이트

        $db->query("update inventory set itemid=$itemid+1, itemlevel=$itemlevel+1, usedtype=$usedtype where id=$id and itemgroup = $itemgroup") or
        die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);
        $result = FindItem($id, $itemgroup);
        if ($result == null || $result->num_rows == 0) die($RESULT_ERROR.'인벤토리 생성은 성공했는데 찾을수가 없다');
        //$result = UpdateInventory($itemgroup, $id, $itemid, $itemlevel, $usedtype);
        //if ($result == null || $result->num_rows == 0) die($RESULT_ERROR.'인벤토리 업데이트 실패');
    }

    //돈 차감
    $db->query("update user set coin=(coin-$price) where id=$id") or die ($RESULT_QUERY_ERROR.'코인 차감 실패,'.$db->error);

    $item = $result->fetch_assoc();
    exit($RESULT_SUCCESS.json_encode($item));
}
exit($RESULT_DO_NOTHING);

//
// 인벤토리 가져오기 ( 여러개 )
//
function FindInventory($id)
{
    global $db;
    global $RESULT_QUERY_ERROR;

    $result = $db->query("select itemgroup, itemid, itemlevel, usedtype from inventory where id = '$id'")
    or die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);

    return $result;
}

//
// 아이템 가져오기 ( 한 개 )
//
function FindItem($id , $itemgroup)
{
    global $db;
    global $RESULT_QUERY_ERROR;

    $result = $db->query("select itemgroup, itemid, itemlevel, usedtype from inventory where id=$id and itemgroup = $itemgroup")
    or die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);

    return $result;
}

//
//
//
function UpdateInventory($itemgroup, $id, $itemid, $itemlevel, $usedtype)
{
    global $db;
    global $RESULT_QUERY_ERROR;

//    $result = $db->query("select itemgroup, itemid, itemlevel, usedtype, userid from inventory where userid = '$userid' and itemgroup = $itemgroup") or
//    die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);
    $result = $db->query("update inventory set itemid=$itemid, itemlevel=$itemlevel, usedtype=$usedtype where id = $id and itemgroup =  $itemgroup") or
        die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);

    return $result;
}

function CheckCoin($id)
{
    global $db;
    global $RESULT_QUERY_ERROR;

    $result = $db->query("select coin from user where id = '$id' limit 1") or
    die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);

    return false;
}

//function UpdateInventory($id, $itemid, $itemlevel, $usedtype)
//{
//    global $db;
//    global $RESULT_QUERY_ERROR;
//    global $RESULT_EMPTY_PARAMS;
//
//    if ($id == null) exit($RESULT_EMPTY_PARAMS.'id');
//    if ($itemid == null) exit($RESULT_EMPTY_PARAMS.'itemid');
//    if ($itemlevel == null) exit($RESULT_EMPTY_PARAMS.'itemlevel');
//    if ($usedtype == null) exit($RESULT_EMPTY_PARAMS.'usedtype');
//
//    $result = $db->query("update inventory set itemid=$itemid, itemlevel=$itemlevel, usedtype=$usedtype where userid=$id") or
//        die ($RESULT_QUERY_ERROR.'찾기 실패'.$db->error);
//    return $result;
//}